import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, Image, TouchableOpacity, View } from 'react-native';

export default class Welcome extends Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.logoContainer}>
            <Image style={styles.logo}
                source={require('../../images/mlogo200.png')}/>
        </View>
        <View style={styles.descriptionContainer}>
            <Text style={styles.description}>O SEU CARTÃO MÁGICO</Text>
        </View>
        <View style={styles.cardContainer}>
            <Image style={styles.card}
                source={require('../../images/ekumbucard.png')}/>
        </View>
        <View style={styles.buttonsContainer}>
            <TouchableOpacity style={styles.button} onPress={this.goToLogin.bind(this)}>
                <Text style={styles.buttonText}>Minha Conta</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.link} onPress={this.goToRegister.bind(this)}>
                <Text style={styles.linkText}>Nova Conta</Text>
            </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
  
  goToLogin() {
    this.props.navigator.push({ 
        screen: 'Login',
    });
  }

  goToRegister() {
    this.props.navigator.push({ screen: 'Register' });
  }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor: '#f4f3ef',
        padding: 20
    },
    logoContainer: {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
        marginTop: 70
    },
    logo: {
        width : 100,
        height: 100
    },
    descriptionContainer : {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
        marginTop: 10
    },
    description : {
        textAlign: 'center',
        opacity: 0.7,
        fontSize: 14,
        width: 170,
        fontWeight : '700'
    },
    cardContainer: {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
        marginTop: 40
    },
    card : {
        height: 160,
        width: 240
    },
    buttonsContainer : {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
        marginTop: 40,
        padding: 10
    },
    button: {
        backgroundColor: '#808080',
        paddingVertical : 10,
        width: 170,
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    link : {
        paddingVertical : 10,
        marginTop: 15
    },
    linkText: {
        color: "black"
    }
})