import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Image, AsyncStorage } from 'react-native';

export default class Success extends Component {
 
  constructor() {
    super();
    this.state = {
        email: '',
        name: ''
    }      

    AsyncStorage.getItem('NewAccount', (err, result) => {
        var accountInfo = JSON.parse(result);
        this.setState({email : accountInfo.email})
        this.setState({name: accountInfo.firstname})
    });
  }

  render() {
    return (
        <View style={styles.container}> 
            <View style={styles.imageContainer}>
                <Image style={styles.image}
                    source={require('../../images/signupsc.png')}/>
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.text}>{this.state.name}, obrigado pelo interesse no ekumbu. Para activar sua conta verifique seu email ({this.state.email}) e clique no link indicado para a activação.</Text>
            </View>
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.button} onPress={this.goToHome.bind(this)}>
                    <Text style={styles.buttonText}>Minha Conta</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
  }
  goToHome() {
    this.props.navigator.push({ screen: 'Home' });
  }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor: '#f4f3ef',
        padding: 20
    },
    imageContainer: {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
        marginTop: 40
    },
    image : {
        height: 110,
        width: 110
    },
    buttonContainer : {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
        marginTop: 40,
        padding: 10
    },
    button: {
        backgroundColor: '#808080',
        paddingVertical : 10,
        width: 170,
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    textContainer: {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
        padding: 10
    },
    text : {
        fontSize: 13,
        justifyContent: 'center',
        textAlign: 'center',
        opacity: 0.7,
        width: 250
    }
})