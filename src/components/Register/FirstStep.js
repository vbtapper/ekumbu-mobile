import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, KeyboardAvoidingView, TextInput, View, Alert, Navigator, ActivityIndicator, AsyncStorage } from 'react-native';

import EkumbuActivityIndicator from "../utils/EkumbuActivityIndicator/EkumbuActivityIndicator"

const config                    = require("../utils/Configuration/config")
export default class FirstStep extends Component {
    
  constructor() {
    super();

    this.state = {
        animating: false,
        email : '',
        password: '',
        cpassword: ''
    }      
  }

  render() {
    return (
        <View style={styles.container}> 
            <View style={styles.titleContainter}>
                <Text style={styles.title}>NOVA CONTA</Text>
                <Text style={styles.description}>Compre online usando o ekumbu abrindo uma conta agora!</Text>
            </View>
            <View> 
                <EkumbuActivityIndicator animating = {this.state.animating} />
            </View>
            <KeyboardAvoidingView behavior="position"> 
            <View style={styles.inputsContainer}> 
                <TextInput 
                    onChangeText={ (text)=> this.setState({email: text}) }
                    placeholder='email'
                    returnKeyType="go"
                    ref="email"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}>
                </TextInput>
                <TextInput 
                    onChangeText={ (text)=> this.setState({password: text}) }
                    secureTextEntry
                    placeholder='senha'
                    returnKeyType="next"
                    ref="password"
                    style={styles.input}>
                </TextInput>
                <TextInput 
                    onChangeText={ (text)=> this.setState({cpassword: text}) }
                    secureTextEntry
                    placeholder='confirmar senha'
                    returnKeyType="next"
                    ref="cpassword"
                    style={styles.input}>
                </TextInput>
            </View>
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.button} onPress={this.goToNext.bind(this)}>
                    <Text style={styles.buttonText}>Continuar</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.link} onPress={this.goToHome.bind(this)}>
                    <Text style={styles.linkText}>Cancelar</Text>
                </TouchableOpacity>
            </View> 
            </KeyboardAvoidingView>
        </View>
    );
  }

  validateEmail(email) {
    var re = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return re.test(email);
  }

  validatePassword(password) {
      return /[A-Z]+/.test(password) && /[a-z]+/.test(password) &&
                    /[\d\W]/.test(password) && /\S{6,}/.test(password);
  }

  goToHome() {
    this.props.navigator.push({ 
        screen: 'Home'
    });   
  }

  async goToNext() {
    if(!this.validateEmail(this.state.email)) {
        Alert.alert('ekumbu', "Digite um email válido");
    }
    else if(!this.validatePassword(this.state.password)) {
        Alert.alert('ekumbu', "Digite uma senha válida. Sua senha deve conter obrigatoriamente uma letra maiscula e um número.");
    }
    else if(!this.validatePassword(this.state.cpassword)) {
        Alert.alert('ekumbu', "Digite a senha confirmação válida e igual a senha. Sua senha deve conter obrigatoriamente uma letra maiscula e um número.");
    }
    else if(this.state.password.trim() != this.state.cpassword.trim()) {
        Alert.alert('ekumbu', "As senhas devem ser iguais");   
    }
    else {
        this.setState({animating: true})
        try {
            let response = await fetch(config.API.baseurl + "/api/v1/customer/chexeml", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        "Authorization" : config.API.key
                    },
                    body: JSON.stringify({
                        email: this.state.email,
                        livemode : config.API.livemode 
                    })
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                if(responseJson.result) {              
                    this.setState({animating: false})
                    Alert.alert('ekumbu', "Já existe uma conta registrada com este email");
                }
                else {
                    try {
                        let NewAccountInfo = {
                            email: this.state.email,
                            password: this.state.password,
                        };

                        AsyncStorage.setItem('NewAccount', JSON.stringify(NewAccountInfo), () => {
                            this.setState({animating: false})
                            this.props.navigator.push({ 
                                screen: 'SecondStep'
                            }); 
                        });
                        
                    } catch (error) {
                        this.setState({animating: false})
                        Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
                    }
                }
            }
            else {
                this.setState({animating: false})
                Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
            }
        } 
        catch(error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");   
        }
    }
  }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor: '#f4f3ef',
        padding: 20
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        marginBottom: 10,
        paddingHorizontal : 10
    },
    titleContainter: {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        justifyContent: 'center',
    },
    description: {
        fontSize: 13,
        marginTop: 10,
        justifyContent: 'center',
        textAlign: 'center',
        opacity: 0.7,
        width: 250
    },
    buttonContainer : {
    
    },
    button: {
        backgroundColor: '#808080',
        paddingVertical : 15,
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    link : {
        paddingVertical : 10,
        marginTop: 15
    },
    linkText: {
        color: "black",
        textAlign: 'center'
    },
    modal : {
        width: 40,
        height: 40,
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
        backgroundColor: 'red'
    }
})