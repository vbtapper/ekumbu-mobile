import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, KeyboardAvoidingView, TextInput, View, AsyncStorage, Alert } from 'react-native';

import EkumbuActivityIndicator from "../utils/EkumbuActivityIndicator/EkumbuActivityIndicator"
const config                    = require("../utils/Configuration/config")

export default class LastStep extends Component {
    
  constructor() {
    super();

    this.state = {
        animating: false
    }      
  }

  render() {
    return (
        <View style={styles.container}> 
            <View style={styles.titleContainter}>
                <Text style={styles.title}>Finalizar</Text>
                <Text style={styles.description}>Obrigado pelo interesse em usar o Ekumbu, clicando em Enviar aceitará e concordorá com as politicas de privacidade e com o contracto de cliente Ekumbu.</Text>
            </View>
            <View> 
                <EkumbuActivityIndicator animating = {this.state.animating} />
            </View>
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.button} onPress={this.goToNext.bind(this)}>
                    <Text style={styles.buttonText}>Enviar</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.link} onPress={this.goToHome.bind(this)}>
                    <Text style={styles.linkText}>Cancelar</Text>
                </TouchableOpacity>
            </View>   
        </View>
    );
  }

  goToHome() {
    this.props.navigator.push({ screen: 'Home' });
  }

  async goToNext() {
    var accountInfo = null;
    await AsyncStorage.getItem('NewAccount', (err, result) => {
        this.setState({animating: true})
        accountInfo = JSON.parse(result);
    });

    try {
        let response = await fetch(config.API.baseurl + "/api/v1/vendor/createcustomer", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization" : config.API.key
            },
            body: JSON.stringify({
                firstname: accountInfo.firstname,
                lastname: accountInfo.lastname,
                phone: accountInfo.phone,
                email: accountInfo.email,
                password: accountInfo.password,
                redirecturl : config.server.baseurl + '/actaccount' + '?email=' + accountInfo.email,
                livemode: config.API.livemode, 
            })
        })
        let responseJson = await response.json();
        if(responseJson.statusCode == 200) {
            this.setState({animating: false})
            this.props.navigator.push({ screen: 'Success' });  
        }
        else {
            this.setState({animating: false})
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
        }
    } 
    catch(error) {
        this.setState({animating: false})
        Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");   
    }
  }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor: '#f4f3ef',
        padding: 20
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        marginBottom: 10,
        paddingHorizontal : 10
    },
    titleContainter: {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        justifyContent: 'center',
    },
    description: {
        fontSize: 13,
        marginTop: 10,
        justifyContent: 'center',
        textAlign: 'center',
        opacity: 0.7,
        width: 300
    },
    buttonContainer : {
    
    },
    button: {
        backgroundColor: '#808080',
        paddingVertical : 15,
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    link : {
        paddingVertical : 10,
        marginTop: 15
    },
    linkText: {
        color: "black",
        textAlign: 'center'
    }
})