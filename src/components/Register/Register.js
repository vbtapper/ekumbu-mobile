import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, KeyboardAvoidingView, Navigator } from 'react-native';

import FirstStep from "./FirstStep"
import SecondStep from "./SecondStep"
import LastStep from "./LastStep"
import Success from "./Success"
import Home from "../Home/Home"


let buildStyleInterpolator = require('buildStyleInterpolator');

var NoTransition = {
  opacity: {
    from: 1,
    to: 1,
    min: 1,
    max: 1,
    type: 'linear',
    extrapolate: false,
    round: 100,
  },
};

export default class Register extends Component {    
    render() {
        return (
            <View style={styles.container}>
                <Navigator
                    initialRoute={{screen: 'FirstStep'}}
                    renderScene={(route, nav) => {return this.renderScene(route, nav)}}
                    configureScene={ () => { 
                    return {
                            ...Navigator.SceneConfigs.HorizontalSwipeJumpFromRight, 
                            gestures: false, 
                            defaultTransitionVelocity: 100,
                            animationInterpolators: {
                            into: buildStyleInterpolator(NoTransition),
                            out: buildStyleInterpolator(NoTransition),
                            },
                        }; 
                    }}
                />
            </View>
        );
    }
    renderScene(route,nav) {
        switch (route.screen) {
        case "FirstStep":
            return <FirstStep navigator={nav} />
        case "SecondStep":
            return <SecondStep navigator={nav} />
        case "LastStep":
            return <LastStep navigator={nav} />
        case "Success":
            return <Success navigator={nav} />
        case "Home":
            return <Home navigator={nav} />
        }
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor: '#f4f3ef',
    }
})