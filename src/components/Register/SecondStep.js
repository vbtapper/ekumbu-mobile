import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, KeyboardAvoidingView, TextInput, View, Alert, AsyncStorage } from 'react-native';

import DatePicker from 'react-native-datepicker'

export default class SecondStep extends Component {

  constructor() {
    super();
    this.state = {
        animating: false,
        fname: '',
        lname: '',
        phone: ''
    }      
  }

  render() {
    return (
        <View style={styles.container}> 
            <View style={styles.titleContainter}>
                <Text style={styles.title}>DADOS PESSOAS</Text>
                <Text style={styles.description}>Seus dados são mantidos completamente seguros!</Text>
            </View>
            <View> 
                <EkumbuActivityIndicator animating = {this.state.animating} />
            </View>
            <KeyboardAvoidingView behavior="position"> 
            <View style={styles.inputsContainer}>
                <TextInput 
                    placeholder='primeiro nome'
                    returnKeyType="go"
                    ref="fname"
                    keyboardType="name-phone-pad"
                    onChangeText={ (text)=> this.setState({fname: text}) }
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}>
                </TextInput>
                <TextInput 
                    placeholder='ultimo nome'
                    returnKeyType="go"
                    ref="lname"
                    keyboardType="name-phone-pad"
                    onChangeText={ (text)=> this.setState({lname: text}) }
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}>
                </TextInput>
                <TextInput 
                    placeholder='telefone'
                    returnKeyType="go"
                    ref="phone"
                    keyboardType="phone-pad"
                    onChangeText={ (text)=> this.setState({phone: text}) }
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}>
                </TextInput>
            </View>
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.button} onPress={this.goToNext.bind(this)}>
                    <Text style={styles.buttonText}>Continuar</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.link} onPress={this.goToHome.bind(this)}>
                    <Text style={styles.linkText}>Cancelar</Text>
                </TouchableOpacity>
            </View>   
            </KeyboardAvoidingView>
        </View>
    );
  }

  validateName(name) {
      return /^[a-zA-Z]+$/.test(name);
  }

  validatePhone(phone) {
    return /^[0-9]{10}$/.test(phone);
  }
  
  goToHome() {
    this.props.navigator.push({ screen: 'Home' });
  }

  goToNext() {
    if(!this.validateName(this.state.fname) || (this.state.fname.toString().length <= 2)) {
        Alert.alert('ekumbu', "Digite o seu primeiro nome conforme no documento de identificação");
    }
    else if(!this.validateName(this.state.lname) || this.state.lname.toString().length <= 2) {
        Alert.alert('ekumbu', "Digite o seu ultimo nome conforme no documento de identificação");
    }
    else if(!this.validatePhone(this.state.phone)) {
        Alert.alert('ekumbu', "Digite o seu número de telefone correctamente");
    }
    else {
        AsyncStorage.getItem('NewAccount', (err, result) => {
            var accountInfo = JSON.parse(result);
            var FinalCustomerInfo = {
                email : accountInfo.email,
                password : accountInfo.password,
                firstname : this.state.fname,
                lastname : this.state.lname,
                phone: this.state.phone
            }
            AsyncStorage.mergeItem('NewAccount', JSON.stringify(FinalCustomerInfo), () => {
                this.props.navigator.push({ screen: 'LastStep' });  
            });
        });
    } 
  }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor: '#f4f3ef',
        padding: 20
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        marginBottom: 10,
        paddingHorizontal : 10
    },
    titleContainter: {
        alignItems : 'center',
        flexGrow : 1,
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        justifyContent: 'center',
    },
    description: {
        fontSize: 13,
        marginTop: 10,
        justifyContent: 'center',
        textAlign: 'center',
        opacity: 0.7,
        width: 250
    },
    buttonContainer : {
    
    },
    button: {
        backgroundColor: '#808080',
        paddingVertical : 15,
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    link : {
        paddingVertical : 10,
        marginTop: 15
    },
    linkText: {
        color: "black",
        textAlign: 'center'
    }
})