const API_VERSION                   = null
const API_URL                       = null
const API_KEY                       = null
const API_LIVE_MODE                 = null

module.exports = {
    server: {
        baseurl:'http://testekb.ekumbu.com',
    },
    API: {
        version: API_VERSION || '0.1.0',
        baseurl : API_URL || 'http://testapi.ekumbu.com',
        key : API_KEY || 'Bearer 345634252492049028459294910950205912934DFHE244',
        livemode : API_LIVE_MODE || 'false'
    },
    mail: {
        ekumbumail : "mail.ekumbu.com",
        mailkey : "key-6c9a2780e8f0a9a0976bd6938e0ffb23"
    },
    rates : {
        API : 'http://kwerapi.ekumbu.com/kwanzaer/getrate'
    },
    deposits : {
        fee : 5,
        url: 'http://deposit.ekumbu.com',
        access_key: 'Bearer deposit-access-token'
    },
    sendmoney : {
        url: 'http://sendmoney.ekumbu.com',
        access_key: 'Bearer send-access-token'
    },
    rates : {
        API : 'http://kwerapi.ekumbu.com/kwanzaer/getrate'
    },
};
