const React = require('react');
const {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  TouchableOpacity
} = require('react-native');
const { Component } = React;

const windowD = Dimensions.get('window');
const uri = 'https://pickaface.net/gallery/avatar/Opi51c74d0125fd4.png';

export default class Menu extends Component {
  static propTypes = {
    onItemSelected: React.PropTypes.func.isRequired,
  };

  constructor(props) {
      super(props);
  }

  render() {
    return (
      <View style={styles.menu}>
        <View style={styles.avatarContainer}>
          <Image
            style={styles.avatar}
            source={{ uri, }}/>
          <Text style={styles.name}>EVANILSON ABRIL</Text>
        </View>
        <TouchableOpacity style={styles.itemContainer} onPress={() => this.props.onItemSelected('first')}>
          <View style={{backgroundColor: '#f4f3ef', padding: 5, width: 50}}>
            <Image style={styles.itemLogo} source={require('../../../images/myekumbu.png')}/>
          </View>
          <View style={{backgroundColor: '#f4f3ef', padding: 9, width: 320}}>
            <Text  style={styles.item}> MEU EKUMBU </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={styles.itemContainer} onPress={() => this.props.onItemSelected('second')}>
          <View style={{backgroundColor: '#f4f3ef', padding: 5, width: 50}}>
            <Image style={styles.itemLogo} source={require('../../../images/transferMenu.png')}/>
          </View>

          <View style={{backgroundColor: '#f4f3ef', padding: 9, width: 320}}>
            <Text style={styles.item}> TRANSAÇÕES </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={styles.itemContainer} onPress={() => this.props.onItemSelected('fith')}>
          <View style={{backgroundColor: '#f4f3ef', padding: 5, width: 50}}>
            <Image style={styles.itemLogo} source={require('../../../images/settings.png')}/>
          </View>
          <View style={{backgroundColor: '#f4f3ef', padding: 9, width: 320}}>
            <Text style={styles.item}> DEFINIÇÕES </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={styles.itemContainer} onPress={() => this.props.onItemSelected('forth')}>
          <View style={{backgroundColor: '#f4f3ef', padding: 5, width: 50}}>
          <Image style={styles.itemLogo} source={require('../../../images/exitmenu.png')}/>
          </View>
          <View style={{backgroundColor: '#f4f3ef', padding: 9, width: 320}}>
            <Text style={styles.item}>SAIR</Text>
          </View>
        </TouchableOpacity>

      </View>
    );
  }
};

const styles = {
  menu: {
    flexGrow: 1,
    width: windowD.width,
    height: windowD.height,
    backgroundColor: '#D1D1BA',
    opacity: 0.9
  },
  avatarContainer: {
    left: 20,
    top: 10,
    marginBottom: 20,
    marginTop: 20,
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    flexGrow: 1,
  },
  name: {
    position: 'absolute',
    fontWeight: '600',
    left: 70,
    top: 20,
  },
  item: {
    fontSize: 15,
    fontWeight: 'bold',
    backgroundColor: '#f4f3ef',
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexGrow: 1,
    marginTop: 7
  },
  itemContainer : {
    marginBottom: 3,
    flexDirection: 'row',
    width: 500,
    height: 50,
    paddingLeft: 0,
  },
  itemLogo: {
    width: 40,
    height: 40,
    backgroundColor: '#f4f3ef',
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexGrow: 1,
    padding: 7
  }
};