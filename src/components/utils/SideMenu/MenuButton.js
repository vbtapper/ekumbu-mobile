import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator, Image } from 'react-native';

class MenuButton extends Component {

  handlePress(e) {
    if (this.props.onPress) {
      this.props.onPress(e);
    }
  }

  addNewService(e) {
    if (this.props.addNewService) {
        this.props.addNewService(e);
    } 
  }

  render() {
    return (
      <View style={styles.menuButton} >
        <View>
            <TouchableOpacity 
                onPress={this.handlePress.bind(this)}
                style={this.props.style}>
                <Text>{this.props.children}</Text>
                <Image
                    source={{ uri: 'http://i.imgur.com/vKRaKDX.png', width: 40, height: 40, }} />        
            </TouchableOpacity>  
        </View>
        <View style={styles.exitButtonArea}>
            <TouchableOpacity style={styles.exitButton} onPress={this.addNewService.bind(this)}>
                <Image source={require('../../../images/plus.png')} style={styles.addButton} />
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = {
    menuButton: {
        flexDirection: 'row',
        marginTop: 20,
        backgroundColor: '#D1D1BA'
    },
    exitButton: {
        marginTop: 10,
    },
    exitButtonArea: {
        alignItems: 'flex-end',
        flex: 1
    },
    exitText: {
        fontSize: 16,
        fontWeight: '600'
    },
    addButton: {
        width: 44,
        height: 44
    }
}

module.exports = MenuButton