import React, { Component } from 'react';
import {
   ActivityIndicator,
   View,
   StyleSheet
} from 'react-native';

export default EkumbuActivityIndicator = (props) => {
   return (
      <View style = {styles.container}>
         <ActivityIndicator animating = {props.animating}
           style = {styles.activityIndicator} size = 'large'
           color = "black"
         />
      </View>
   );
}

const styles = StyleSheet.create ({
   container: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 20,
   },
   activityIndicator: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 100,
      width: 80
   }
});