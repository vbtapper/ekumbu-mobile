'use strict';

import React from 'react';
import { StyleSheet } from 'react-native';

const hairlineWidth = StyleSheet.hairlineWidth;

export const BG_COLOR = '#f4f3ef';


export default StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        backgroundColor: '#f4f4f4'
    },
    main: {
        flex: 1,
        alignSelf: 'flex-end'
    },
    row: {
        flexDirection: 'row'
    }
});


export const keyStyle = StyleSheet.create({
    wrapper: {
        flex: 1,
        height: 48,
        backgroundColor: '#f4f3ef'
    },
    bd: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: hairlineWidth,
        borderTopWidth: hairlineWidth,
        borderColor: '#f4f3ef'
    },
    border: {
      borderColor: '#f4f3ef'
    },
    mainText: {
        fontSize: 20,
        color: '#000'
    },
    otherText: {
        fontSize: 10,
        color: '#333',
    },
    bg_d2d5dc: {
        backgroundColor: BG_COLOR
    },
    bgLessL: {
      backgroundColor: '#f4f3ef'
    },
    dot: {
        height: 30,
        fontSize: 20,
        lineHeight: 25
    }
});