import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, KeyboardAvoidingView, TouchableOpacity, TextInput, Alert, AsyncStorage, Modal, Dimensions } from 'react-native';

const config                    = require("../utils/Configuration/config")
const Spinner                   = require('react-native-spinkit');
const windowDimensions          = Dimensions.get('window');

export default class Login extends Component {
  constructor() {
    super();

    this.state = {
        animating: false,
        email : '',
        password: '',
        types: ['CircleFlip', 'Bounce', 'Wave', 'WanderingCubes', 'Pulse', 'ChasingDots', 'ThreeBounce', 'Circle', '9CubeGrid', 'WordPress', 'FadingCircle', 'FadingCircleAlt', 'Arc', 'ArcAlt'],
        size: 20,
        color: "black",
        isVisible: false,
        loginButtonEnabled: false,
    }      
  }

  render() {
  
    return (
      <View style={styles.container}>
            <TouchableOpacity style={styles.exitButton} onPress={this.goToWelcome.bind(this)}>
                <Image style={styles.exit} source={require('../../images/exit.png')}/>
            </TouchableOpacity>
            <KeyboardAvoidingView behavior="position" style={styles.formContainer}> 
                <View style={styles.logoContainer}>
                    <Image style={styles.logo}
                        source={require('../../images/mlogo200.png')}/>
                    <Text style={styles.description}>Insira suas credências e acesse sua conta</Text>
                </View>
                <View style={styles.textInputContainer}>
                    <TextInput 
                        placeholder='email'
                        returnKeyType="next"
                        onSubmitEditing={() => this.passwordInput.focus()}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        onChangeText={ (text)=> this.setState({email: text}) }
                        autoCorrect={false}
                        style={styles.input}>
                    </TextInput>
                    <TextInput 
                        secureTextEntry
                        placeholder='senha'
                        returnKeyType="go"
                        onChangeText={ (text)=> this.setState({password: text}) }
                        ref={(input) => this.passwordInput = input}
                        style={styles.input}>
                    </TextInput>
                </View>
                <View style={{width: windowDimensions.width-100, alignSelf: 'center'}}>
                    <TouchableOpacity style={styles.buttonContainer} onPress={this.Login.bind(this)} disabled={this.state.loginButtonEnabled}>
                        <View style={{alignItems: 'flex-end', flex: 1}}>
                            <Text style={styles.buttonText}>ENTRAR</Text>
                        </View>
                        <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}> 
                            <Spinner style={styles.spinner} isVisible={this.state.isVisible} size={this.state.size} type={'FadingCircle'} color={this.state.color}/>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonForgotPassword}>
                        <Text style={styles.buttonForgotPasswordText}>Esqueceu a Senha ?</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
      </View>
    );
  }

  validateEmail(email) {
    var re = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return re.test(email);
  }

  isEmpty(value) {
    return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
  }

  async Login() {
    this.setState({loginButtonEnabled: true})

    if(!this.validateEmail(this.state.email)) {
        Alert.alert('ekumbu', "Digite um email válido");
    }
    else if(this.isEmpty(this.state.password)) {
        Alert.alert('ekumbu', "Digite sua senha");
    }
    else {
        this.setState({animating: true, isVisible: true})
        try {
            let response = await fetch(config.API.baseurl + "/api/v1/customer/signin", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        "Authorization" : config.API.key
                    },
                    body: JSON.stringify({
                        email: this.state.email,
                        password: this.state.password,
                        livemode: config.API.livemode,
                        ipaddress : "0.0.0.0"
                    })
            })
            let responseJson = await response.json();

            if(responseJson.statusCode == 200) {
        
                try {
                    let LoggedCustomer = {
                        sessionToken: responseJson.sessiontoken
                    };

                    AsyncStorage.setItem('LoggedCustomer', JSON.stringify(LoggedCustomer), () => {
                        this.setState({animating: false, modalVisible: false})
                        this.props.navigator.push({ 
                            screen: 'Dashboard' 
                        }); 
                    });
                    
                } catch (error) {
                    this.setState({animating: false, isVisible: false, loginButtonEnabled: false})
                    Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
                }
                
            }
            else if(responseJson.statusCode == 403) {
                this.setState({animating: false, isVisible: false, loginButtonEnabled: false})
                Alert.alert('ekumbu', "Email ou Senha incorrecta, verifique seus dados");
            }
            else {
                this.setState({animating: false, isVisible: false, loginButtonEnabled: false})
                Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
            }
        } 
        catch(error) {
            this.setState({animating: false, isVisible: false, loginButtonEnabled: false})
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");   
        }
    }
  }

  goToWelcome() {
    this.props.navigator.push({ screen: 'Welcome' });
  }
}

const styles = {
    container : {
        flex : 1,
        backgroundColor: '#f4f3ef'
    },
    logoContainer : {
      alignItems : 'center',
      marginTop: 10
    },
    logo: {
      width : 100,
      height: 100
    },
    description: {
      marginTop: 5,
      textAlign: 'center',
      opacity: 0.7,
      fontSize: 14,
      width: 160,
    },
    exitButton :{
      marginTop: 40,
      flex: 0,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      marginRight : 20
    },
    exit :{
      flex: 0,
      width: 30,
      height: 30
    },
    buttonContainer : {
        backgroundColor: '#92918F',
        paddingVertical : 15,
        shadowColor: '#D1D1BA',
        shadowOffset: {
            width: 1,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 1.0,
        flexDirection: 'row'
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    buttonForgotPassword: {
        paddingVertical: 10,
        marginTop: 5
    },
    buttonForgotPasswordText : {
        textAlign: 'center',
        fontSize : 12,
        opacity: 0.7
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        marginBottom: 10,
        paddingHorizontal : 10
    },
    textInputContainer: {
        padding: 20
    },
    spinner: {
        alignSelf: 'center',
    },
}