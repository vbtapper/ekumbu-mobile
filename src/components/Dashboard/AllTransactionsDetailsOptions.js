import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator, Image, Alert, AsyncStorage, Picker, Modal } from 'react-native';

import AllTransactionsOptionsMenu from './AllTransactionsOptionsMenu'
import CalendarPicker from '../../components/utils/CalendarPicker/CalendarPicker'
import moment from 'moment';
import SelectMultiple from 'react-native-select-multiple'

const config = require("../utils/Configuration/config");

// --- OR ---
 const searchOptions = [
   { label: 'Todas', value: 'all' },
   { label: 'Pagamentos', value: 'payment' },
   { label: 'Depositos', value: 'deposit' },
   { label: 'Transferencias', value: 'transfer' },
   { label: 'Envios', value: 'send' }
 ]

class AllTransactionsDetailsOptions extends Component {

    constructor(props) {
        super(props);
         this.state = {
            selectedStartDate: '',
            selectedEndDate: '',
            selectedOptions: []
        }
        this.onDateChange = this.onDateChange.bind(this);
    }

  componentDidMount() {
        try {
            AsyncStorage.getItem('TransactionOptions', (err, result) => {
                var transactionOptions = JSON.parse(result);
                this.setState({selectedStartDate: transactionOptions.selectedStartDate, selectedEndDate: transactionOptions.selectedEndDate, selectedOptions: transactionOptions.selectedOptions})
            });
                
        }
        catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 1");
        }
    }

  BackToMain() {
    this.props.navigator.replace({id: 'alltransactions'})
  }

  ChangeOptions() {
      try {
            let transactionOptions = {
                selectedOptions: this.state.selectedOptions,
                selectedStartDate: this.state.selectedStartDate,
                selectedEndDate: this.state.selectedEndDate,
            };

            AsyncStorage.setItem('TransactionOptions', JSON.stringify(transactionOptions), () => {
                this.props.navigator.push({id: 'alltransactions'})
            });
            
        } catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
        }
  }

  onTypeChange(option) {

  }

  onDateChange(date, type) {
    if (type === 'END_DATE') {
      this.setState({
        selectedEndDate: date,
      });
    } else {
      this.setState({
        selectedStartDate: date,
        selectedEndDate: null,
      });
    }
  }

  onSelectionsChange = (selectedOptions) => {
    this.setState({ selectedOptions })
  }

  render() {

    var date = new Date();
    date.setFullYear( date.getFullYear() - 10 );

    const { selectedStartDate, selectedEndDate } = this.state;
    const minDate =  date;
    const maxDate = new Date();
    const startDate  =  selectedStartDate ? selectedStartDate.toString() : '';
    const endDate = selectedEndDate ? selectedEndDate.toString() : '';

    return (  
      <View style={styles.page}>
            <View style={styles.detailMenu}>
                <AllTransactionsOptionsMenu handlePressBack={() => this.BackToMain()} handlePressSave={() => this.ChangeOptions()}/>
            </View>
            <ScrollView style={{padding: 20}}> 
                <View style={{marginBottom: 15}}>
                    <View style={{marginBottom: 10}}>
                        <Text style={styles.headerText}>Selecione o intervalo da data</Text>
                    </View>
                    <View style={{padding: 30}}>
                        <CalendarPicker
                            startFromMonday={true}
                            allowRangeSelection={true}
                            minDate={minDate}
                            maxDate={maxDate}
                            todayBackgroundColor="#f2e6ff"
                            selectedDayColor="#7300e6"
                            selectedDayTextColor="#FFFFFF"
                            onDateChange={this.onDateChange}
                        />
                    </View>
                </View>
                <View>
                    <View style={{marginBottom: 10}}>
                        <Text style={styles.headerText}>Selecione os tipos de transação</Text>
                    </View>
                    <View style={{padding: 10, backgroundColor: '#f4f3ef'}}>
                        <SelectMultiple
                            items={searchOptions}
                            style={{backgroundColor: '#f4f3ef', margin: 3}}
                            selectedItems={this.state.selectedOptions}
                            onSelectionsChange={this.onSelectionsChange} />
                    </View>
                <View>
                </View>
                </View>
            </ScrollView>
        </View> 
    );
  }

  async GetTransactionDetail() {
        var adtUrl = "/api/v1/customer/gevtdetail?" + "sessiontoken=" + this.state.sessionToken + "&livemode=" + config.API.livemode + "&srcID=" + this.state.detailsID
        try {
            let response = await fetch(config.API.baseurl +  adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                },
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                this.setState({
                    amountCharged: responseJson.amountCharged,
                    amountReceived: responseJson.amountReceived,
                    date: responseJson.dateProcess,
                    location: responseJson.sourceInfo.last4,
                    fee: responseJson.fee,
                    status: responseJson.status,
                    trackingID: responseJson.trackingID,
                    rate: responseJson.rate,
                })
            }
            else {
                Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 1');
            }
        } 
        catch(error) {
            Alert.alert(error.message)
            Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 2');   
        }
    }  
}

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    transactionSection: {
        flex: 1
    },
    transDetailsBanner: {
        flex: 0,
        width: window.width,
        height: 200,
        backgroundColor: '#E6E6D9',
        alignItems : 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        opacity: 0.9
    },
    transDetailsBannerLineTwo: {
        fontSize: 9,
        marginTop: 5,
        color: 'black',
        fontWeight: 'bold'
    },
    transDetailsBannerLineOne: {
        fontSize: 11,
        color: 'black',
        marginTop: 8,
        fontWeight: 'bold'
    },
    transDetailsBannerIcon: {
        height: 60,
        width: 60,       
    },
    transDetailsFrom: {
        padding: 10,
        flexDirection: 'row',
        width: window.width,
        justifyContent: 'space-around',
    },
    detailMenu: {
        width: window.width,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    transDetailsFromColumnLabel: {
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignItems: 'flex-start',
        flex: 1
    },

    transDetailsFromColumnValue: {
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flex: 1
    },
    separator: {
        height: 1,
        backgroundColor: '#dddddd'
    },

    transDetailsValue: {
        fontSize: 12
    },
    headerText: {
        fontSize: 14,
    }
}

module.exports = AllTransactionsDetailsOptions