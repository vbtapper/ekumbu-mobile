import React, { Component } from 'react';
import { Text, Dimensions, View, ScrollView, TouchableOpacity, Image } from 'react-native';

import ServiceMenu from "../ServiceMenu"
const windowDimensions = Dimensions.get('window');

class ChooseDepositType extends Component {

    constructor(props) {
        super(props);

        this.state = {
            backroute: 'chooseservice'
        }     
    }

    componentDidMount() {
        
    }

    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    GoToKioskyDeposit() {
        this.props.navigator.replace({id: 'kioskydeposit'})
    }

    GoToCreditCardDeposit() {
        this.props.navigator.replace({id: 'enterdetailsccdeposit'})
    }

    render() {
        return (
            <View style={styles.page}>
                <View style={styles.serviceMenu}>
                    <ServiceMenu onPress={() => this.BackToMain()}/>
                </View>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={styles.MainLayout}>
                        <View style={{alignItems: 'center', marginBottom: 25}}>
                            <Text style={{fontSize: 10, fontWeight: 'bold'}}>SELECIONE A ORIGEN DEPOSITO</Text>
                        </View>
                        <View style={styles.optionsLayout}>
                            <View style={{marginRight: 15}}>
                                <TouchableOpacity style={styles.optionsButtons} onPress={() => this.GoToKioskyDeposit()}>
                                    <Image source={require('../../../../images/kiosk.png')} style={styles.optionLogo} />
                                    <Text style={{fontSize: 9}}>Quiosque</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{marginRight: 15}}>
                                <TouchableOpacity style={styles.optionsButtons}>
                                    <Image source={require('../../../../images/multicaixa.png')} style={styles.optionLogo} />
                                    <Text style={{fontSize: 9}}>Multicaixa</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.optionsLayout}>
                            <View style={{marginRight: 15}}>
                                <TouchableOpacity style={styles.optionsButtons} onPress={() => this.GoToCreditCardDeposit()}>
                                    <Image source={require('../../../../images/ccd.png')} style={styles.optionLogo} />
                                    <Text style={{fontSize: 9}}>Visa</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{marginRight: 15}}>
                                <TouchableOpacity style={styles.optionsButtons}>
                                    <Image source={require('../../../../images/paypal.png')} style={styles.optionLogo} />
                                    <Text style={{fontSize: 9}}>PayPal</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

module.exports = ChooseDepositType

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    serviceMenu: {
        width: window.width,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    MainLayout: {
        flexDirection: 'column',
        justifyContent: 'center',
        width: windowDimensions.width,
        height: windowDimensions.height,
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
    },
    optionsButtons: {
        width: 80,
        height: 80,
        paddingBottom: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#808080',
        borderWidth: 1,
        borderRadius: 50,
        flexDirection: 'column'
    },
    optionLogo: {
        width: 30,
        height: 30,
        alignSelf: 'center'
    },
    optionsLayout: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        width: window.innerWidth,
        padding: 10
    }
}
