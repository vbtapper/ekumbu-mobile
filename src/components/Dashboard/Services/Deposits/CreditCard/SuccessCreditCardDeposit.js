import React, { Component } from 'react';
import { AppRegistry, Text, View, Dimensions, ScrollView, AsyncStorage } from 'react-native';

const windowDimensions = Dimensions.get('window');
var currencyFormatter       = require('currency-formatter');
import ServiceMenu from "../../ServiceMenu"

class SuccessCreditCardDeposit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            backroute: 'first',
            total: '',
            currency: '',
            trackingID: ''
        }  
    }

    componentDidMount() {
        AsyncStorage.getItem('CreditCardDeposit', (err, result) => {
            var ccDeposit = JSON.parse(result);
            this.setState({trackingID: ccDeposit.trackingID, total: ccDeposit.total, currency: ccDeposit.currency})
        });
    }
    
    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    render() {
        return (
            <View style={styles.page}>
                <View>
                    <ServiceMenu onPress={() => this.BackToMain()}/>
                </View>
                <ScrollView style={{flex: 1, flexDirection: 'column'}}> 
                    <View style={styles.areaLayout}>
                        <Text style={{fontSize: 12}}>
                            Deposito efectuado com sucesso. Foi retirado {currencyFormatter.format(this.state.total, { symbol: this.state.currency,  format: "%v %s" })}. O código de rastreamento para o deposito é {this.state.trackingID}
                        </Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

module.exports = SuccessCreditCardDeposit

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    amountToSendLayout: {
        alignItems: 'center',
        marginTop: 10,
        padding: 10,
        width: window.outerWidth,
        flex: 1,
        flexDirection: 'column'
    },
    buttonContainer : {
        backgroundColor: '#92918F',
        paddingVertical : 15,
        shadowColor: '#D1D1BA',
        shadowOffset: {
            width: 1,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 1.0,
        flexDirection: 'row'
    },
    SimpleButtonContainer: {
        backgroundColor: '#92918F',
        paddingVertical : 15,
        shadowColor: '#D1D1BA',
        shadowOffset: {
            width: 1,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 1.0,
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    modalCloseIcon: {
        width: 20,
        height: 20
    },
    currencyIcon: {
        width: 25,
        height: 25,
    },
    separator: {
        height: 1,
        backgroundColor: 'black'
    },
    areaLayout: {
        width: windowDimensions.width - 50,
        alignSelf: 'center',
        flexDirection: 'column',
        padding: 10,
        marginTop: 3,
        borderRadius: 5, 
        flex: 1,
    },
    editButtonText: {
      textDecorationLine: "underline",
      textDecorationStyle: "solid",
      textDecorationColor: "#000",
      fontSize: 9
    },
    input: {
        height: 40,
        width: windowDimensions.width-30,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        marginBottom: 10,
        paddingHorizontal : 10
    },
}