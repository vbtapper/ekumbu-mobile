import React, { Component } from 'react';
import { AppRegistry, Text, View, ScrollView, Dimensions, Picker, TouchableOpacity, Modal, Image, Alert , AsyncStorage} from 'react-native';

import ServiceMenu from "../../ServiceMenu"
const windowDimensions = Dimensions.get('window');
const config = require("../../../../utils/Configuration/config");
const EkumbuNumericalPad = require('../../../../utils/KeyboardPad/NumericalPad');
var currencyFormatter = require('currency-formatter');

var currencyOpts = [
    {
        id : 1,
        code: 'usd',
        label: 'USD',
        name: 'Dolar Americano',
        symbol: '$',
        icon: require('../../../../../images/dollar.png')
    },
    {
        id : 2,
        code: 'euro',
        label: 'EUR',
        name: 'Euro',
        symbol: '€',
        icon: require('../../../../../images/euro.png')
    },
    {
        id : 3,
        code: 'real',
        label: 'BRL',
        name: 'Real Brasileiro',
        symbol: 'R$',
        icon: require('../../../../../images/real.png')
    }
]

let model = {
    
    _keys: [],

    _listeners: [],

    addKey(key) {
        this._keys.push(key);
        this._notify();
    },

    delKey() {
        this._keys.pop();
        this._notify();
    },

    clearAll() {
        this._keys = [];
        this._notify();
    },

    getKeys() {
        return this._keys;
    },

    onChange(listener) {
        if (typeof listener === 'function') {
            this._listeners.push(listener);
        }
    },

    _notify() {
        this._listeners.forEach((listner) => {
            listner(this);
        });
    }
};

class EnterDepositDetails extends Component {

    constructor(props) {
        super(props);

        this.state = {
            backroute: 'choosedepositservice',
            currency: 'USD',
            amountToSend: currencyFormatter.format(0, { code: 'USD' }),
            currencySymbol: '$',
            currencyCode: 'USD',
            realAmount: 0,
            showModal: false
        }  
    }

    componentDidMount() {
        model.clearAll();
    }

    _handleClear() {
        model.clearAll();
    }
 
    _handleDelete() {
        model.delKey();
    }
 
    _handleKeyPress(key) {
        model.addKey(key);
    }

    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    ShowCurrencyOptions() {
        this.setState({
            showModal: true
        })
    }

    HideCurrencyOption() {
        this.setState({
            showModal: false
        })
    }

    selectCurrency(labelParam, symbolParam, context) {
        context.setState({
            currencyCode: labelParam,
            currencySymbol: symbolParam,
            currency: labelParam,
            amountToSend: currencyFormatter.format(context.state.realAmount, { code: labelParam }),
            showModal: false
        })
    }

    cmdNextAction() {
        if(parseInt(this.state.realAmount) > 0) {
            try {
                let ccDeposit = {
                    amount: parseInt(this.state.realAmount),
                    currency: this.state.currency
                };

                AsyncStorage.setItem('CreditCardDeposit', JSON.stringify(ccDeposit), () => {
                    this.props.navigator.replace({id: 'submitccdeposit'})
                });
                
            } catch (error) {
                Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
            }
        }
    }

    renderCurrencyOptions(contextParam) {
        return currencyOpts.map(function(opt, i) {
            return (
                <View key={i}>
                    <TouchableOpacity style={{flexDirection: 'row', flex: 1, padding: 10,}} onPress={() => contextParam.selectCurrency(opt.label, opt.symbol, contextParam)}>
                        <View style={{alignItems: 'flex-start'}}>
                            <Image source={opt.icon} style={styles.currencyIcon}  />
                        </View>
                        
                        <View style={{flex: 1, alignItems: 'flex-start', alignSelf: 'center'}}>
                            <Text style={{marginLeft: 5, fontSize: 14, flex: 1, alignItems: 'center', }}>{opt.name}</Text>
                        </View>
                        
                        <View style={{flex: 1, alignItems: 'flex-end', alignSelf: 'center'}}>
                            <Text style={{flex: 1, fontSize: 12, alignItems: 'center'}}>{opt.label}</Text>
                        </View>
                    </TouchableOpacity>
                </View>   
            )
        });
    }

    render() {

        model.onChange((model) => {
            this.setState(
                {
                    amountToSend: currencyFormatter.format(model.getKeys().join(''), { code: this.state.currencyCode }),
                    realAmount: model.getKeys().join('')
                }
            );
        });

        return (
            <View style={styles.page}>
                <View>
                    <ServiceMenu onPress={() => this.BackToMain()}/>
                </View>
                <ScrollView style={{flex: 1, flexDirection: 'column'}}>
                    <View style={styles.amountToSendLayout}>
                        <View style={{alignItems: 'flex-start', flex: 1, justifyContent: 'center', flexDirection: 'row'}}>
                            <Text style={{fontSize: 36, alignSelf: 'flex-end'}}>
                                {this.state.amountToSend}
                            </Text>
                        </View>

                        <View style={{alignItems: 'flex-start', flex: 1, marginLeft: 5, justifyContent: 'center', marginTop: 20}}>
                            <TouchableOpacity onPress={() => this.ShowCurrencyOptions()}>
                                <Text>{this.state.currency} ></Text>
                            </TouchableOpacity>
                            <Modal
                                animationType={"slide"}
                                transparent={true}
                                visible={this.state.showModal}>
                                <View>
                                    <ScrollView style={{marginTop: 70, width: windowDimensions.width-50, height: windowDimensions.height-50, backgroundColor: '#f4f3ef', alignSelf: 'center', flexDirection: 'column', borderWidth: 0, borderColor: 'black'}}>
                                        <View style={{flexDirection: 'row', padding: 2, flex: 1, }}>
                                            <View style={{alignItems: 'flex-start'}}>
                                                <TouchableOpacity onPress={() => this.HideCurrencyOption()}>
                                                    <Image source={require('../../../../../images/xclose.png')} style={styles.modalCloseIcon}  />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{alignItems: 'center', flex: 1}}>
                                                <Text style={{fontSize: 12, alignSelf: 'center'}}>Escolha a Moeda</Text>
                                            </View>
                                        </View>

                                        {this.renderCurrencyOptions(this)}
                                    </ScrollView>
                                </View>
                            </Modal>
                        </View>
                    </View>
                    <View style={{flex: 1, width: windowDimensions.width-100, marginTop: 50, alignSelf: 'center'}}>
                        <EkumbuNumericalPad 
                            keyboardType="decimal-pad"
                            onClear={this._handleClear.bind(this)}
                            onDelete={this._handleDelete.bind(this)}
                            onKeyPress={this._handleKeyPress.bind(this)}
                        />
                    </View>

                    <View style={{flex: 1, marginTop: 30, width: windowDimensions.width-100, alignSelf: 'center'}}>
                        <TouchableOpacity style={styles.buttonContainer} onPress={() => this.cmdNextAction()}>
                            <Text style={styles.buttonText}>Seguinte</Text>
                        </TouchableOpacity>
                    </View>
                    
                </ScrollView>
            </View>
        );
    }
}

module.exports = EnterDepositDetails

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    amountToSendLayout: {
        alignItems: 'center',
        marginTop: 10,
        padding: 10,
        width: window.outerWidth,
        flex: 1,
        flexDirection: 'column'
    },
    buttonContainer : {
        backgroundColor: '#92918F',
        paddingVertical : 15
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    modalCloseIcon: {
        width: 20,
        height: 20
    },
    currencyIcon: {
        width: 25,
        height: 25,
    },
    separator: {
        height: 1,
        backgroundColor: 'black'
    },
}