import React, { Component } from 'react';
import { AppRegistry, Text, View, Dimensions, ScrollView, TouchableOpacity, AsyncStorage, Alert, Modal, Image, TextInput } from 'react-native';

import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import ServiceMenu from "../../ServiceMenu"

const windowDimensions = Dimensions.get('window');
const config                = require("../../../../utils/Configuration/config");
var currencyFormatter       = require('currency-formatter');
const Spinner               = require('react-native-spinkit');

function TruckString(value, n) {
    return (value.length > n) ? value.substr(0, n-1) + '...' : value;
}

cardLabels = {
    number : 'Número do Cartão',
    expiry: "Exp. Data", 
    cvc: "Cvc/Ccv"
}

class SubmitCreditCardDeposit extends Component {
 
  constructor(props) {
        super(props);

        this.state = {
            sessiontoken: '',
            backroute: 'enterdetailsccdeposit',
            description: '',
            statement: '',
            currency: '',
            amountToSend: 0,
            foundRate: 0,
            fee: 0,
            totalToPay: 0,
            amountToReceive: '',
            showEditCard: false,
            showEditDescription : false,
            cardInfoNumber: '',
            cardInfoExpDate: '',
            cardInfoCvc: '',
            cardInfoType: '',
            last4: '',
            ekumbuDescription: '',
            statementDescription : '',
            isSpinnerVisible: false
        }  
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem('CreditCardDeposit', (err, result) => {
                var ccDeposit = JSON.parse(result);
                this.setState({currency: ccDeposit.currency, amountToSend: ccDeposit.amount})
                this.GetRate() 
            });
            AsyncStorage.getItem('LoggedCustomer', (err, result) => {
                var LoggedCustomer = JSON.parse(result);
                this.setState({sessiontoken: LoggedCustomer.sessionToken})
            });
        }
        catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 1");
        }
    }
    
    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    render() {
        return (
          <View style={styles.page}>
              <View>
                  <ServiceMenu onPress={() => this.BackToMain()}/>
              </View>
              <ScrollView style={{flex: 1, flexDirection: 'column'}}>
                  <View style={styles.labelLayout}>
                      <Text style={{fontSize: 9}}>A Receber</Text>
                  </View>
                  <View style={styles.areaLayout}>
                      <View style={{flexDirection: 'row', flex: 1}}>
                            <View style={{alignItems: 'flex-start', flex: 1}}>
                                <Text style={{fontSize: 12}}>Valor a Receber</Text>
                            </View>
                            <View style={{alignItems: 'flex-end', flex: 1}}>
                                <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.amountToReceive, { symbol: "AOA",  format: "%v %s" })}</Text>
                            </View>
                      </View>

                      <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                            <View style={{alignItems: 'flex-start', flex: 1}}>
                                <Text style={{fontSize: 12}}>Cambio</Text>
                            </View>
                            <View style={{alignItems: 'flex-end', flex: 1}}>
                                <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.foundRate, { symbol: "AOA",  format: "%v %s" })}</Text>
                            </View>
                      </View>
                  </View>

                  <View style={styles.labelLayout}>
                      <Text style={{fontSize: 9}}>A Depositar</Text>
                  </View>
                  <View style={styles.areaLayout}>
                      <View style={{flexDirection: 'row', flex: 1}}>
                            <View style={{alignItems: 'flex-start', flex: 1}}>
                                <Text style={{fontSize: 12}}>Percentagem</Text>
                            </View>
                            <View style={{alignItems: 'flex-end', flex: 1}}>
                                <Text style={{fontSize: 13}}>5%</Text>
                            </View>
                      </View>
                      <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                            <View style={{alignItems: 'flex-start', flex: 1}}>
                                <Text style={{fontSize: 12}}>Total</Text>
                            </View>
                            <View style={{alignItems: 'flex-end', flex: 1}}>
                                <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.amountToSend, { symbol: this.state.currency,  format: "%v %s" })}</Text>
                            </View>
                      </View>
                      <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                            <View style={{alignItems: 'flex-start', flex: 1}}>
                                <Text style={{fontSize: 12}}>Taxa</Text>
                            </View>
                            <View style={{alignItems: 'flex-end', flex: 1}}>
                                <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.fee, { symbol: this.state.currency,  format: "%v %s" })}</Text>
                            </View>
                      </View>
                      <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                            <View style={{alignItems: 'flex-start', flex: 1}}>
                                <Text style={{fontSize: 12}}>Sub-Total</Text>
                            </View>
                            <View style={{alignItems: 'flex-end', flex: 1}}>
                                <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.totalToPay, { symbol: this.state.currency,  format: "%v %s" })}</Text>
                            </View>
                      </View>
                  </View>

                  <View style={styles.labelLayout}>
                      <Text style={{fontSize: 9}}>Descrições do Deposito</Text>
                  </View>
                  <View style={styles.areaLayout}>
                      <View style={{flexDirection: 'row', flex: 1}}>
                            <View style={{alignItems: 'flex-start', width: 60}}>
                                <Text style={{fontSize: 12}}>Descrição</Text>
                            </View>
                            <View style={{alignItems: 'flex-start', flex: 1}}>
                                <Text style={{fontSize: 13, marginLeft: 5}}>{TruckString(this.state.description, 30)}</Text>
                            </View>
                      </View>
                      <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                            <View style={{alignItems: 'flex-start', width: 60}}>
                                <Text style={{fontSize: 12}}>Extrato</Text>
                            </View>
                            <View style={{alignItems: 'flex-start', flex: 1}}>
                                <Text style={{fontSize: 13, marginLeft: 5}}>{TruckString(this.state.statement, 30)}</Text>
                            </View>
                            <View style={{alignItems: 'flex-end', width:26}}>
                                <TouchableOpacity style={{alignSelf: 'center'}} onPress={() => this.ShowEditDescription()}>
                                    <Text style={styles.editButtonText}>editar</Text>
                                </TouchableOpacity>
                            </View>
                            <Modal
                                animationType={"slide"}
                                transparent={true}
                                visible={this.state.showEditDescription}>
                                <View>
                                    <ScrollView style={{marginTop: 60, width: windowDimensions.width-30, height: windowDimensions.height-50, backgroundColor: '#f4f3ef', alignSelf: 'center', flexDirection: 'column', borderWidth: 0, borderColor: 'black'}}>
                                        <View style={{flexDirection: 'row', padding: 10, flex: 1, }}>
                                            <View style={{alignItems: 'flex-start'}}>
                                                <TouchableOpacity onPress={() => this.HideEditDescription()}>
                                                    <Image source={require('../../../../../images/xclose.png')} style={styles.modalCloseIcon}  />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{alignItems: 'center', flex: 1}}>
                                                <Text style={{fontSize: 12, alignSelf: 'center'}}>Descrições do Deposito</Text>
                                            </View>
                                        </View>
                                        <View style={{flexDirection: 'column', flex: 1, padding: 10}}>
                                            <View>
                                                <TextInput 
                                                    placeholder='Descrição Ekumbu'
                                                    returnKeyType="next"
                                                    keyboardType="email-address"
                                                    autoCapitalize="none"
                                                    onChangeText={ (text)=> this.setState({ekumbuDescription: text}) }
                                                    autoCorrect={false}
                                                    style={styles.input}>
                                                </TextInput>
                                            </View>

                                            <View>
                                                <TextInput 
                                                    placeholder='Descrição extrato bancario'
                                                    returnKeyType="next"
                                                    keyboardType="email-address"
                                                    autoCapitalize="none"
                                                    onChangeText={ (text)=> this.setState({statementDescription: text}) }
                                                    autoCorrect={false}
                                                    style={styles.input}>
                                                </TextInput>
                                            </View>

                                            <View style={{marginTop: 5, flex: 1, padding: 10, width: windowDimensions.width-100, alignSelf: 'center'}}>
                                                <TouchableOpacity style={styles.SimpleButtonContainer} onPress={() => this.SaveDescriptions()}>
                                                    <Text style={styles.buttonText}>Ok</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </ScrollView>
                                </View>
                            </Modal>
                      </View>
                  </View>

                  <View style={styles.labelLayout}>
                      <Text style={{fontSize: 9}}>Informações do Cartão</Text>
                  </View>
                  <View style={styles.areaLayout}>
                      <View style={{flexDirection: 'row', flex: 1}}>
                            <View style={{alignItems: 'flex-start', width: 60}}>
                                <Text style={{fontSize: 12}}>Cartão</Text>
                            </View>
                            <View style={{alignItems: 'flex-start', flex: 1}}>
                                <Text style={{fontSize: 13, marginLeft: 5}}>{this.state.last4} {this.state.cardInfoType}</Text>
                            </View>
                            <View style={{alignItems: 'flex-end', width:30}}>
                                <TouchableOpacity onPress={() => this.ShowEditCard()}>
                                    <Text style={styles.editButtonText}>editar</Text>
                                </TouchableOpacity>
                            </View>
                      </View>
                      <Modal
                            animationType={"slide"}
                            transparent={true}
                            visible={this.state.showEditCard}>
                            <View>
                                <ScrollView style={{marginTop: 60, width: windowDimensions.width-30, height: windowDimensions.height-50, backgroundColor: '#f4f3ef', alignSelf: 'center', flexDirection: 'column', borderWidth: 0, borderColor: 'black'}}>
                                    <View style={{flexDirection: 'row', padding: 10, flex: 1, }}>
                                        <View style={{alignItems: 'flex-start'}}>
                                            <TouchableOpacity onPress={() => this.HideEditCard()}>
                                                <Image source={require('../../../../../images/xclose.png')} style={styles.modalCloseIcon}  />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{alignItems: 'center', flex: 1}}>
                                            <Text style={{fontSize: 12, alignSelf: 'center'}}>Informações do Cartão</Text>
                                        </View>
                                    </View>
                                    <View style={{flexDirection: 'column', flex: 1, padding: 10}}>
                                        <View>
                                            <CreditCardInput onChange={this._onCreditCardChange} labels={cardLabels}/>
                                        </View>

                                        <View style={{marginTop: 5, flex: 1, padding: 10, width: windowDimensions.width-100, alignSelf: 'center'}}>
                                            <TouchableOpacity style={styles.SimpleButtonContainer} onPress={() => this.SaveCreditInformation()}>
                                                <Text style={styles.buttonText}>Ok</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </Modal>
                  </View>

                  <View style={{flex: 1, marginTop: 20, width: windowDimensions.width-100, alignSelf: 'center'}}>
                        <TouchableOpacity style={styles.buttonContainer} onPress={() => this.MakeDeposit()}>
                            <View style={{alignItems: 'flex-end', flex: 1}}>
                                <Text style={styles.buttonText}>Depositar</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}> 
                                <Spinner style={{alignSelf: 'center'}} isVisible={this.state.isSpinnerVisible} size={20} type={'FadingCircle'} color={'black'}/>
                            </View>
                        </TouchableOpacity>
                    </View>
              </ScrollView>
          </View>
        );
    }

    _onCreditCardChange = form => {
        this.setState({
            cardInfoNumber: form.values.number,
            cardInfoExpDate: form.values.expiry,
            cardInfoCvc: form.values.cvc,
            cardInfoType: "(" + form.values.type + ")"
        })
    }

    SaveCreditInformation() {
        var cnumber = this.state.cardInfoNumber;
        this.setState({
            showEditCard: false,
            last4 : "x-" + cnumber.substr(cnumber.length-4, cnumber.length)
        })
    }

    ShowEditCard() {
        this.setState({
            showEditCard: true
        })
    }

    HideEditCard() {
        this.setState({
            showEditCard: false
        })
    }

    ShowEditDescription() {
         this.setState({
            showEditDescription: true
        })
    }

    HideEditDescription() {
         this.setState({
            showEditDescription: false
        })
    }

    SaveDescriptions() {
        this.setState({
            showEditDescription: false,
            statement: this.state.statementDescription,
            description: this.state.ekumbuDescription
        })
    }

    async MakeDeposit() {
        
        this.setState({
            isSpinnerVisible: true
        })

        var cardExpDate = this.state.cardInfoExpDate
        var cardMonth = cardExpDate.substr(0,2)
        var cardYear = cardExpDate.substr(3,4)

        try {
            let response = await fetch(config.deposits.url + "/dfcard", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.deposits.access_key
                },
                body: JSON.stringify({
                    cardNumber : this.state.cardInfoNumber.replace(/\s/g,''),
                    expmonth : cardMonth,
                    expyear : cardYear,
                    cvv : this.state.cardInfoCvc,
                    sessiontoken : this.state.sessiontoken,
                    amount : this.state.totalToPay,
                    amouttransfer : currencyFormatter.format(this.state.amountToReceive, { symbol: 'AOA',  format: "%v %s" }).toString(),
                    livemode: config.API.livemode,
                    ipaddress : '0.0.0.0',
                    description : this.state.description,
                    statementdescription : this.state.statement,
                    currency : this.state.currency,
                    amountToEkb : this.state.amountToReceive.toString(),
                    rate : this.state.foundRate,
                    fee: this.state.fee.toString()
                })
            });
            
            let responseJson = await response.json();
            this.setState({
                isSpinnerVisible: false
            })

            if(responseJson.statusCode == 200) {
                try {
                    let depositInfo = {
                        total: this.state.totalToPay,
                        currency: this.state.currency,
                        trackingID: responseJson.trackingID,
                    };
                    
                    AsyncStorage.mergeItem('CreditCardDeposit', JSON.stringify(depositInfo), () => {
                        this.props.navigator.replace({id: 'successccdeposit'})
                    });
                } catch (error) {
                    Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
                }
                
            }
            else {
                if(responseJson.message === "Customer Invalid Card or Card Expired" || responseJson.message === "Invalid Card or Insuficient Funds") {
                    Alert.alert('ekumbu', "Não há fundos suficientes no cartão para completar o deposito");
                }
                else if (responseJson.message === "Internal Error Creating Deposit, but the charge was made" || responseJson.message === "Internal Error Creating Token, but the charge was made")  {
                    Alert.alert('ekumbu', "Seus fundos foram transferidos mas ocorreu um erro interno na finalização do deposito, entraremos em contacto consigo para finalizar o deposito. Por favor não efectuar um novo deposito, sem antes entrarmos em comunicação");
                }
                else {
                    Alert.alert('ekumbu', responseJson.message);
                 }
            }
        } 
        catch(error) {
            Alert.alert('ekumbu', error.message);   
        }
    }

    async GetRate() {
        var adtUrl = "?from=" + this.state.currency
        try {
            let response = await fetch(config.rates.API + adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                var calcFee = (this.state.amountToSend*config.deposits.fee)/100;
                var calcAmountToPay = calcFee+this.state.amountToSend
                var calcAmountToReceive = responseJson.rate * this.state.amountToSend
                console.log(calcAmountToReceive.toString())
                this.setState({
                    fee : calcFee,
                    totalToPay: calcAmountToPay,
                    foundRate: responseJson.rate,
                    amountToReceive: calcAmountToReceive
                })
            }
            else {
                Alert.alert('ekumbu', responseJson.message);
            }
        } 
        catch(error) {
            Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 2');   
        }
    }
}


module.exports = SubmitCreditCardDeposit

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    amountToSendLayout: {
        alignItems: 'center',
        marginTop: 10,
        padding: 10,
        width: window.outerWidth,
        flex: 1,
        flexDirection: 'column'
    },
    buttonContainer : {
        backgroundColor: '#92918F',
        paddingVertical : 15,
        shadowColor: '#D1D1BA',
        shadowOffset: {
            width: 1,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 1.0,
        flexDirection: 'row'
    },
    SimpleButtonContainer: {
        backgroundColor: '#92918F',
        paddingVertical : 15,
        shadowColor: '#D1D1BA',
        shadowOffset: {
            width: 1,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 1.0,
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    modalCloseIcon: {
        width: 20,
        height: 20
    },
    currencyIcon: {
        width: 25,
        height: 25,
    },
    separator: {
        height: 1,
        backgroundColor: 'black'
    },
    areaLayout: {
        width: windowDimensions.width - 50,
        alignSelf: 'center',
        flexDirection: 'column',
        padding: 10,
        marginTop: 3,
        borderRadius: 5, 
        borderWidth: 1,
        flex: 1,
        backgroundColor: '#f4f3ef',
        shadowColor: '#D1D1BA',
        shadowOffset: {
        width: 1,
        height: 1
      },
      shadowRadius: 1,
      shadowOpacity: 1.0
    },
    labelLayout: {
      width: windowDimensions.width - 50,
      alignSelf: 'center',
      flexDirection: 'column',
      padding: 10,
      marginTop: 10,
      flex: 1,
    },
    editButtonText: {
      textDecorationLine: "underline",
      textDecorationStyle: "solid",
      textDecorationColor: "#000",
      fontSize: 9
    },
    input: {
        height: 40,
        width: windowDimensions.width-30,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        marginBottom: 10,
        paddingHorizontal : 10
    },
}