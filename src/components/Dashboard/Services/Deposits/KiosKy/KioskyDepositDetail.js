import React, { Component } from 'react';
import { AppRegistry, Text, View, Dimensions, ScrollView, Image, Animated, StyleSheet, AsyncStorage, Alert } from 'react-native';

import ServiceMenu from "../../ServiceMenu"
const windowDimensions = Dimensions.get('window');
import MapView, {Marker} from 'react-native-maps';

const config = require("../../../../utils/Configuration/config");

const ASPECT_RATIO = windowDimensions.width / windowDimensions.height;
const LATITUDE = -8.9898515;
const LONGITUDE = 13.288669000000027;
const LATITUDE_DELTA = 0.0722;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class KioskyDepositDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            kioskyID: '',
            backroute: 'kioskydeposit',
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            marker: {
                latlng: {
                    latitude: 0,
                    longitude: 0
                },
                title: 'kero kilamba',
                description: 'deposite para o seu ekumbu'
            },
            name: '',
            townCity: '',
            hours: '',
            street: ''
        }     
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem('kioskyDetail', (err, result) => {
                var kioskyDetail = JSON.parse(result);
                this.setState({kioskyID: kioskyDetail.id})
                this.GetVendorDetail() 
            });
           
        }
        catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 1");
        }
        
    }

    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    onRegionChange(regionParam) {
        this.setState({region : regionParam});
    }

    render() {
        return (
            <View style={styles.page}>
                <View style={styles.serviceMenu}>
                    <ServiceMenu onPress={() => this.BackToMain()}/>
                </View>
                <View style={styles.transDetailsBanner}>
                    <Image source={require('../../../../../images/kiosky.png')} style={styles.transDetailsBannerIcon} />
                    <Text style={styles.transDetailsBannerLineOne}>{this.state.name}</Text>
                    <Text style={styles.transDetailsBannerLineTwo}>{this.state.townCity}</Text>
                    <Text style={styles.transDetailsBannerLineTwo}>{this.state.street}</Text>
                    <Text style={styles.transDetailsBannerLineTwo}>{this.state.hours}</Text>
                </View>
                <ScrollView>
                    <MapView style={styles.map} initialRegion={this.state.region} region={this.state.region} >
                        <MapView.Marker
                            coordinate={this.state.marker.latlng}
                            title={this.state.marker.title}
                            description={this.state.marker.description}
                        />
                    </MapView>
                </ScrollView>
            </View>
        );
    }

    async GetVendorDetail() {
        var adtUrl = "/api/v1/vendor/getvendordetail?" + "vendorID=" + this.state.kioskyID
        try {
            let response = await fetch(config.API.baseurl +  adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                },
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                this.setState({
                    region: {
                        latitude: responseJson.vendorDetails.latitude,
                        longitude: responseJson.vendorDetails.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    },
                    marker: {
                        latlng: {
                            latitude: responseJson.vendorDetails.latitude,
                            longitude: responseJson.vendorDetails.longitude
                        },
                        title: responseJson.vendorDetails.name,
                        description: 'deposite para o seu ekumbu'
                    },
                    name: responseJson.vendorDetails.name,
                    townCity: responseJson.vendorDetails.townCity,
                    street: responseJson.vendorDetails.street,
                    hours: responseJson.vendorDetails.openHour + " - " + responseJson.vendorDetails.closeHour
                })
            }
            else {
                Alert.alert('ekumbu', responseJson.message);
            }
        } 
        catch(error) {
            Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 2');   
        }
    }  
}

module.exports = KioskyDepositDetail

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    transDetailsBanner: {
        flex: 0,
        width: window.width,
        height: 200,
        backgroundColor: '#E6E6D9',
        alignItems : 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        opacity: 0.9
    },
    transDetailsBannerIcon: {
        height: 60,
        width: 60,       
    },
    transDetailsBannerLineTwo: {
        fontSize: 9,
        marginTop: 5,
        color: 'black',
        fontWeight: 'bold'
    },
    transDetailsBannerLineOne: {
        fontSize: 11,
        color: 'black',
        marginTop: 8,
        fontWeight: 'bold'
    },
    map: {
        width: window.outerWidth,
        height: windowDimensions.height,
    },
}
