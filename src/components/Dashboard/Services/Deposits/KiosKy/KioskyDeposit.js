import React, { Component } from 'react';
import { AppRegistry, Text, View, Dimensions, ListView, ScrollView, TouchableOpacity, Image, Picker, Alert, AsyncStorage } from 'react-native';

import ServiceMenu from "../../ServiceMenu"
const windowDimensions = Dimensions.get('window');

const config = require("../../../../utils/Configuration/config");

import ModalDropdown from 'react-native-modal-dropdown';

var demoData = [{
    name: 'Kero Kilamba',
    street: 'kilamba rua 3',
    municipy: 'Kilamba, Luanda',
    status: 'open'
},
{
    name: 'Shoprit belas shoping',
    street: 'Talatona rua aB 34',
    municipy: 'Ingombotas, Luanda',
    status: 'close'
}]

class KioskyDeposit extends Component {

    constructor(props) {
        super(props);

        this.state = {
            backroute: 'choosedepositservice',
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            province: 'luanda',
            canada: 'Luanda'
        }     
    }

    componentDidMount() {
        this.GetVendors()      
    }

    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    _getOptionList() {
        return this.refs['OPTIONLIST'];
    }

    _canada(province) {
        this.setState({
            ...this.state,
            canada: province
        });
    }

    render() {
        return (
            <View style={styles.page}>
                <View style={styles.serviceMenu}>
                    <ServiceMenu onPress={() => this.BackToMain()}/>
                </View>
                <ScrollView style={styles.listViewLayout}> 
                    <View>
                        <Text style={{fontSize: 10, marginBottom: 3}}>Selecione a Provincia</Text>
                    </View>
                    <View style={{width: window.outerWidth}}>
                        <ModalDropdown options={['Luanda', 'Malange']} defaultValue={'Luanda'} style={{width: window.outerWidth,  borderWidth: 1, height: 35}} animated={false} textStyle={{alignSelf: 'center', fontSize: 15, marginTop: 7, justifyContent: 'center', alignItems: 'center'}} dropdownStyle={{width: windowDimensions.width-20, marginTop: 10}}/>
                    </View>
                    <View>
                        <ListView dataSource={this.state.dataSource} renderRow={this.renderVendors} style={styles.listView} automaticallyAdjustContentInsets={false}/>
                    </View>

                </ScrollView>
            </View>
        );
    }

    KioskyDetails(kID) {
        try {
            let kioskyDetail = {
                id: kID,
            };

            AsyncStorage.setItem('kioskyDetail', JSON.stringify(kioskyDetail), () => {
                this.props.navigator.replace({id: 'kioskydepositdetails'})
            });
            
        } catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
        }        
    }

    renderVendors = (vendor) => {
        var statusString = null;

        if(vendor.status === 'open') {
            statusString = 'Aberto'
        }
        else {
            statusString = 'Fechado'
        }

        return (
            <TouchableOpacity onPress={() => this.KioskyDetails(vendor.id)}>
                <View>
                    <View style={styles.separator}/>
                    <View style={styles.rowContainer}>          
                        <View style={styles.locationInfo}>
                            <View>
                                <Text style={{fontSize: 12}}>{vendor.name}</Text>
                                <Text style={{fontSize: 12}}>{vendor.town}, {vendor.province}</Text>
                            </View>
                        </View>
                        <View style={styles.locationHours} >
                            <View>
                                <Text style={{fontSize: 12}}>{vendor.openHour} - {vendor.closeHour}</Text>
                            </View>
                            <View>
                                <Text style={{fontSize: 12}}>{statusString}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.separator}/>
                </View>
            </TouchableOpacity>
        );
    } 

    async GetVendors() {
        var adtUrl = "/api/v1/vendor/getvendors"
        try {
            let response = await fetch(config.API.baseurl +  adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                },
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseJson.vendors)
                })
            }
            else {
                Alert.alert('ekumbu', responseJson.message);
            }
        } 
        catch(error) {
            Alert.alert(error.message)
            Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 2');   
        }
    }
}

module.exports = KioskyDeposit

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    listViewLayout: {
        width: windowDimensions.width,
        height: windowDimensions.height,
        padding: 10
    },
    listView: {
        flex: 1,
        height: window.innerHeight
    },
    locationInfo: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        flex: 1
    },
    rowContainer: {
        width: window.outerWidth,
        flexDirection: 'row',
        flex: 1,
        padding: 7
    },
    locationHours: {
        flexDirection: 'column',
        alignItems: 'flex-end',
        flex: 1   
    },
    statusIcon: {
        width: 30,
        height: 30
    },
    chooseCity: {
        width: window.outerWidth,
        padding: 5
    },
    optionList: {
        width: window.outerWidth,
        height: window.outerHeight,
        marginTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    }
}
