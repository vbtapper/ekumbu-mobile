import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator, Image } from 'react-native';

class ServiceMenu extends Component {

  handlePress(e) {
    if (this.props.onPress) {
      this.props.onPress(e);
    }
  }

  render() {
    return (
      <View style={styles.menuButton} >
        <View>
            <TouchableOpacity onPress={this.handlePress.bind(this)}>
                <Image style={styles.btIcon} source={require('../../../images/back.png')}/>        
            </TouchableOpacity>  
        </View>
      </View>
    );
  }
}

const styles = {
    menuButton: {
        flexDirection: 'row',
        marginTop: 20,
        backgroundColor: '#D1D1BA',
        height: 40
    },
    exitButton: {
        marginTop: 25,
        marginRight: 5
    },
    exitButtonArea: {
        alignItems: 'flex-end',
        flex: 1
    },
    exitText: {
        fontSize: 16,
        fontWeight: '600'
    },
    btIcon: {
        width: 30,
        height: 30,
        marginTop: 5
    }  
}

module.exports = ServiceMenu