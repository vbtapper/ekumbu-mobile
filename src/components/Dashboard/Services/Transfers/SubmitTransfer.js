import React, { Component } from 'react';
import { AppRegistry, Text, View, ScrollView, TouchableOpacity, Dimensions, AsyncStorage, Alert, TextInput, Modal, Image } from 'react-native';

import ServiceMenu from "../ServiceMenu"

const windowDimensions = Dimensions.get('window');
const config                        = require("../../../utils/Configuration/config");
const EkumbuNumericalPad            = require('../../../utils/KeyboardPad/NumericalPad');
const currencyFormatter             = require('currency-formatter');
const Spinner                       = require('react-native-spinkit');

function TruckString(value, n) {
    return (value.length > n) ? value.substr(0, n-1) + '...' : value;
}

var CountriesToSend = [{
    index: 0,
    name : 'Angola',
    flag: require('../../../../images/angola.png'),
    fee: 0,
    currency : 'AOA',
},
{
    index: 1,
    name: 'Brasil',
    flag: require('../../../../images/brazil.png'),
    fee: 3,
    currency : 'BRL',
},
{
    index: 2,
    name: 'Portugal',
    flag: require('../../../../images/portugal.png'),
    fee: 3,
    currency : 'EUR',
},
{
    index: 3,
    name: 'Estados Unidos da America',
    flag: require('../../../../images/usa.png'),
    fee: 3,
    currency : 'USD',
}];

var BanksByCountry = {
    "0": {
        'banks' : [{
            name: 'Banco Angolano de Investimentos, S.A.',
            logo: require('../../../../images/bai.jpg'),
        },
        {
            name : 'Banco Angolano de Negócios e Comércio, S.A.',
            logo: require('../../../../images/banc.jpg'),
        },
        {
            name : 'Banco BAI Microfinanças, SA.',
            logo: require('../../../../images/bmf.jpg'),
        },
        {
            name: 'Banco BIC, S.A.',
            logo: require('../../../../images/bic.jpg'),
        },
        {
            name : 'Banco de Fomento Angola, S.A.',
            logo: require('../../../../images/bfa.jpg'),
        },
        {
            name : 'Banco de Poupanca e Credito, S.A.',
            logo: require('../../../../images/bpc.jpg'),
        },
        {
            name : 'Banco de Comércio e Indústria, S.A.',
            logo: require('../../../../images/bci.jpg'),
        },
        {
            name : 'Banco Sol, S.A.',
            logo: require('../../../../images/bsol.jpg'),
        }]
    },
    "1" : {
        'banks' : [{
            name: 'Banco do Brasil',
            logo: require('../../../../images/bb.png'),
        },
        {
            name: 'Banco Caixa Geral',
            logo: require('../../../../images/bancocaixageral.png'),
        }]
    },
    "2" : {
        'banks' : [{
            name: 'Millennium BCP',
            logo: require('../../../../images/millenniumbcp.png'),
        },
        {
            name: 'Banco Popular Portugues',
            logo: require('../../../../images/bancopopular.jpg'),
        }]
    },
    "3" : {
        'banks' : [{
            name: 'JPMorgan Chase & Co.',
            logo: require('../../../../images/chase.png'),
        },
        {
            name : 'Bank of America Corp.',
            logo: require('../../../../images/bankofamerica.png'),
        },
        {
            name: 'Citigroup Inc.',
            logo: require('../../../../images/citybank.png'),
        },
        {
            name: 'Wells Fargo & Co.',
            logo: require('../../../../images/wellsfargo.png'),
        }]
    }
}

function findById(source, id) {
    return source.filter(function( obj ) {
        // coerce both obj.id and id to numbers 
        // for val & type comparison
        return +obj.index === +id;
    })[ 0 ];
}

class SubmitTransfer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sessiontoken: '',
            showCountriesModal : false,
            showBanksModal: false,
            showReceiverInfoModal: false,
            showDescriptionModal: false,
            backroute: 'entertransferamount',
            originalAmount: '',
            bank: '',
            countryIndex: 0,
            countryFee: 0,
            country: 'Angola',
            receiverName: '',
            receiverBankAccount: '',
            receiverNameModal: '',
            receiverBankAccountModal: '',
            descriptionModal: '',
            description: '',
            currency: 'AOA',
            amountToSend: 0,
            foundRate: 0,
            fee: 0,
            totalToPay: 0,
            amountToReceive: 0,
            showEditCard: false,
            showEditDescription : false,
            cardInfoNumber: '',
            cardInfoExpDate: '',
            cardInfoCvc: '',
            cardInfoType: '',
            last4: '',
            ekumbuDescription: '',
            statementDescription : '',
            isSpinnerVisible: false
        }  
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem('TransferToAccount', (err, result) => {
                var transferObj = JSON.parse(result);
                this.setState({amountToSend: transferObj.amountToSend, amountToReceive: transferObj.amountToSend, originalAmount: transferObj.amountToSend})
            });
            AsyncStorage.getItem('LoggedCustomer', (err, result) => {
                var LoggedCustomer = JSON.parse(result);
                this.setState({sessiontoken: LoggedCustomer.sessionToken})
            });
        }
        catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 1");
        }
    }

    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    renderCountriesToSend(contextParam) {
        return CountriesToSend.map(function(opt, i) {
            return (
                <View key={i}>
                    <TouchableOpacity style={{flexDirection: 'row', flex: 1, padding: 10,}} onPress={() => contextParam.SelectCountryToSend(opt.index, opt.name, opt.currency, opt.fee)}>
                        <View style={{alignItems: 'flex-start'}}>
                            <Image source={opt.flag} style={styles.flagIcon}  />
                        </View>
                        
                        <View style={{flex: 1, alignItems: 'flex-start', alignSelf: 'center'}}>
                            <Text style={{marginLeft: 5, fontSize: 14, alignItems: 'center', }}>{opt.name}</Text>
                        </View>
                        
                        <View style={{flex: 1, alignItems: 'flex-end', alignSelf: 'center'}}>
                            <Text style={{flex: 1, fontSize: 12, alignItems: 'center'}}>{opt.currency}</Text>
                        </View>
                    </TouchableOpacity>
                </View>   
            )
        });
    }

    ShowChooseCountry() {
        this.setState({
            showCountriesModal: true
        })
    }

    HideChooseCountry() {
        this.setState({
            showCountriesModal: false
        })
    }

    SelectCountryToSend(countryIndexParam, countryNameParam, currencyParam, feeParam) {
        this.setState({
            bank: '',
            currency: currencyParam,
            country: countryNameParam,
            countryIndex: countryIndexParam,
            showCountriesModal: false,
            countryFee: feeParam
        })
        this.GetRate(currencyParam, feeParam)
    }

    renderBanksByCountry(contextParam) {
        
        var banksArray = BanksByCountry[this.state.countryIndex].banks;

        return banksArray.map(function(opt, i) {
            return (
                <View key={i}>
                    <TouchableOpacity style={{flexDirection: 'row', flex: 1, padding: 10}} onPress={() => contextParam.SelectBank(opt.name)}>
                        <View style={{alignItems: 'flex-start', width: 30}}>
                            <Image source={opt.logo} style={styles.banksIcon}  />
                        </View>
                        
                        <View style={{flex: 1, alignItems: 'flex-start', alignSelf: 'center', justifyContent: 'center'}}>
                            <Text style={{marginLeft: 15, fontSize: 12, alignItems: 'center', }}>{opt.name}</Text>
                        </View>
                    </TouchableOpacity>
                </View>   
            )
        });
    }

    ShowChooseBank() {
        this.setState({
            showBanksModal: true
        })
    }

    HideChooseBank() {
        this.setState({
            showBanksModal: false
        })
    }

    SelectBank(bankParam) {
        this.setState({
            showBanksModal: false,
            bank: bankParam
        })
    }

    ShowReceiverInfo() {
        this.setState({
            showReceiverInfoModal: true
        })
    }

    HideReceiverInfo() {
        this.setState({
            showReceiverInfoModal: false
        })
    }

    SaveReceiverInfo() {
        this.setState({
            receiverName: this.state.receiverNameModal,
            receiverBankAccount: this.state.receiverBankAccountModal,
            showReceiverInfoModal: false
        })
    }

    ShowEditDescription() {
        this.setState({
            showDescriptionModal: true
        })
    }

    HideEditDescription() {
        this.setState({
            showDescriptionModal: false
        })
    }

    SaveDescriptions() {
        this.setState({
            description: this.state.descriptionModal,
            showDescriptionModal: false
        })
    }

    render() {
        return (
            <View style={styles.page}>
                <View>
                    <ServiceMenu onPress={() => this.BackToMain()}/>
                </View>
                <ScrollView style={{flex: 1, flexDirection: 'column'}}>
                    <View style={styles.labelLayout}>
                        <Text style={{fontSize: 9}}>Destino</Text>
                    </View>
                    <View style={styles.areaLayout}>
                        <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{alignItems: 'flex-start', width: 60}}>
                                    <Text style={{fontSize: 12}}>Pais</Text>
                                </View>
                                <View style={{alignItems: 'flex-start', flex: 1}}>
                                    <Text style={{fontSize: 13, marginLeft: 5}}>{TruckString(this.state.country, 30)}</Text>
                                </View>
                                <View style={{alignItems: 'flex-end', width:26}}>
                                    <TouchableOpacity style={{alignSelf: 'center'}} onPress={() => this.ShowChooseCountry()}>
                                        <Text style={styles.editButtonText}>editar</Text>
                                    </TouchableOpacity>
                                </View>
                                <Modal
                                    animationType={"slide"}
                                    transparent={true}
                                    visible={this.state.showCountriesModal}>
                                    <View>
                                        <ScrollView style={{marginTop: 60, width: windowDimensions.width-30, height: windowDimensions.height-50, backgroundColor: '#f4f3ef', alignSelf: 'center', flexDirection: 'column', borderWidth: 0, borderColor: 'black'}}>
                                            <View style={{flexDirection: 'row', padding: 10, flex: 1, }}>
                                                <View style={{alignItems: 'flex-start'}}>
                                                    <TouchableOpacity onPress={() => this.HideChooseCountry()}>
                                                        <Image source={require('../../../../images/xclose.png')} style={styles.modalCloseIcon}  />
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{alignItems: 'center', flex: 1}}>
                                                    <Text style={{fontSize: 12, alignSelf: 'center'}}>Selecione o Pais</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection: 'column', flex: 1, padding: 10}}>
                                                {this.renderCountriesToSend(this)}
                                            </View>
                                        </ScrollView>
                                    </View>
                                </Modal>
                        </View>
                        <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                                <View style={{alignItems: 'flex-start', width: 60}}>
                                    <Text style={{fontSize: 12}}>Banco</Text>
                                </View>
                                <View style={{alignItems: 'flex-start', flex: 1}}>
                                    <Text style={{fontSize: 13, marginLeft: 5}}>{TruckString(this.state.bank, 30)}</Text>
                                </View>
                                <View style={{alignItems: 'flex-end', width:26}}>
                                    <TouchableOpacity style={{alignSelf: 'center'}} onPress={() => this.ShowChooseBank()}>
                                        <Text style={styles.editButtonText}>editar</Text>
                                    </TouchableOpacity>
                                </View>
                                <Modal
                                    animationType={"slide"}
                                    transparent={true}
                                    visible={this.state.showBanksModal}>
                                    <View>
                                        <ScrollView style={{marginTop: 60, width: windowDimensions.width-30, height: windowDimensions.height-50, backgroundColor: '#f4f3ef', alignSelf: 'center', flexDirection: 'column', borderWidth: 0, borderColor: 'black'}}>
                                            <View style={{flexDirection: 'row', padding: 10, flex: 1, }}>
                                                <View style={{alignItems: 'flex-start'}}>
                                                    <TouchableOpacity onPress={() => this.HideChooseBank()}>
                                                        <Image source={require('../../../../images/xclose.png')} style={styles.modalCloseIcon}  />
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{alignItems: 'center', flex: 1}}>
                                                    <Text style={{fontSize: 12, alignSelf: 'center'}}>Selecione o Pais</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection: 'column', flex: 1, padding: 10}}>
                                                {this.renderBanksByCountry(this)}
                                            </View>
                                        </ScrollView>
                                    </View>
                                </Modal>
                            </View>
                    </View>

                    <View style={styles.labelLayout}>
                        <Text style={{fontSize: 9}}>Informações do Receptor</Text>
                    </View>

                    <View style={styles.areaLayout}>
                        <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{alignItems: 'flex-start', width: 60}}>
                                    <Text style={{fontSize: 12}}>Nome</Text>
                                </View>
                                <View style={{alignItems: 'flex-start', flex: 1}}>
                                    <Text style={{fontSize: 13, marginLeft: 5}}>{TruckString(this.state.receiverName, 30)}</Text>
                                </View>
                        </View>
                        <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                                <View style={{alignItems: 'flex-start', width: 60}}>
                                    <Text style={{fontSize: 12}}>Conta Bancária</Text>
                                </View>
                                <View style={{alignItems: 'flex-start', flex: 1}}>
                                    <Text style={{fontSize: 13, marginLeft: 5}}>{TruckString(this.state.receiverBankAccount, 30)}</Text>
                                </View>
                                <View style={{alignItems: 'flex-end', width:26}}>
                                    <TouchableOpacity style={{alignSelf: 'center'}} onPress={() => this.ShowReceiverInfo()}>
                                        <Text style={styles.editButtonText}>editar</Text>
                                    </TouchableOpacity>
                                </View>
                                <Modal
                                    animationType={"slide"}
                                    transparent={true}
                                    visible={this.state.showReceiverInfoModal}>
                                    <View>
                                        <ScrollView style={{marginTop: 60, width: windowDimensions.width-30, height: windowDimensions.height-50, backgroundColor: '#f4f3ef', alignSelf: 'center', flexDirection: 'column', borderWidth: 0, borderColor: 'black'}}>
                                            <View style={{flexDirection: 'row', padding: 10, flex: 1, }}>
                                                <View style={{alignItems: 'flex-start'}}>
                                                    <TouchableOpacity onPress={() => this.HideReceiverInfo()}>
                                                        <Image source={require('../../../../images/xclose.png')} style={styles.modalCloseIcon}  />
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{alignItems: 'center', flex: 1}}>
                                                    <Text style={{fontSize: 12, alignSelf: 'center'}}>Informações do Receptor</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection: 'column', flex: 1, padding: 10}}>
                                                <View>
                                                    <TextInput 
                                                        placeholder='Nome'
                                                        returnKeyType="next"
                                                        keyboardType="ascii-capable"
                                                        autoCapitalize="none"
                                                        onChangeText={ (text)=> this.setState({receiverNameModal: text}) }
                                                        autoCorrect={false}
                                                        style={styles.input}>
                                                    </TextInput>
                                                </View>

                                                <View>
                                                    <TextInput 
                                                        placeholder='Conta Bncaria'
                                                        returnKeyType="next"
                                                        keyboardType="number-pad"
                                                        autoCapitalize="none"
                                                        onChangeText={ (text)=> this.setState({receiverBankAccountModal: text}) }
                                                        autoCorrect={false}
                                                        style={styles.input}>
                                                    </TextInput>
                                                </View>

                                                <View style={{marginTop: 5, flex: 1, padding: 10, width: windowDimensions.width-100, alignSelf: 'center'}}>
                                                    <TouchableOpacity style={styles.SimpleButtonContainer} onPress={() => this.SaveReceiverInfo()}>
                                                        <Text style={styles.buttonText}>Ok</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>

                                        </ScrollView>
                                    </View>
                                </Modal>
                            </View>
                        </View>

                        <View style={styles.labelLayout}>
                            <Text style={{fontSize: 9}}>Descrição da Transferencia</Text>
                        </View>

                        <View style={styles.areaLayout}>
                            <View style={{flexDirection: 'row', flex: 1}}>
                                    <View style={{alignItems: 'flex-start', width: 60}}>
                                        <Text style={{fontSize: 12}}>Descrição</Text>
                                    </View>
                                    <View style={{alignItems: 'flex-start', flex: 1}}>
                                        <Text style={{fontSize: 13, marginLeft: 5}}>{TruckString(this.state.description, 30)}</Text>
                                    </View>
                                    <View style={{alignItems: 'flex-end', width:26}}>
                                        <TouchableOpacity style={{alignSelf: 'center'}} onPress={() => this.ShowEditDescription()}>
                                            <Text style={styles.editButtonText}>editar</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Modal
                                        animationType={"slide"}
                                        transparent={true}
                                        visible={this.state.showDescriptionModal}>
                                        <View>
                                            <ScrollView style={{marginTop: 60, width: windowDimensions.width-30, height: windowDimensions.height-50, backgroundColor: '#f4f3ef', alignSelf: 'center', flexDirection: 'column', borderWidth: 0, borderColor: 'black'}}>
                                                <View style={{flexDirection: 'row', padding: 10, flex: 1, }}>
                                                    <View style={{alignItems: 'flex-start'}}>
                                                        <TouchableOpacity onPress={() => this.HideEditDescription()}>
                                                            <Image source={require('../../../../images/xclose.png')} style={styles.modalCloseIcon}  />
                                                        </TouchableOpacity>
                                                    </View>
                                                    <View style={{alignItems: 'center', flex: 1}}>
                                                        <Text style={{fontSize: 12, alignSelf: 'center'}}>Descrição da Transferencia</Text>
                                                    </View>
                                                </View>
                                                <View style={{flexDirection: 'column', flex: 1, padding: 10}}>
                                                    <View>
                                                        <TextInput 
                                                            placeholder='Descrição'
                                                            returnKeyType="next"
                                                            keyboardType="ascii-capable"
                                                            autoCapitalize="none"
                                                            onChangeText={ (text)=> this.setState({descriptionModal: text}) }
                                                            autoCorrect={false}
                                                            style={styles.input}>
                                                        </TextInput>
                                                    </View>

                                                    <View style={{marginTop: 5, flex: 1, padding: 10, width: windowDimensions.width-100, alignSelf: 'center'}}>
                                                        <TouchableOpacity style={styles.SimpleButtonContainer} onPress={() => this.SaveDescriptions()}>
                                                            <Text style={styles.buttonText}>Ok</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </ScrollView>
                                        </View>
                                    </Modal>
                            </View>
                        </View>
                        <View style={styles.labelLayout}>
                            <Text style={{fontSize: 9}}>Valores da Transferencia</Text>
                        </View>

                        <View style={styles.areaLayout}>

                            <View style={{flexDirection: 'row', flex: 1}}>
                                    <View style={{alignItems: 'flex-start', flex: 1}}>
                                        <Text style={{fontSize: 12}}>Taxa</Text>
                                    </View>
                                    <View style={{alignItems: 'flex-end', flex: 1}}>
                                        <Text style={{fontSize: 13}}>{this.state.countryFee}%</Text>
                                    </View>
                            </View>

                            <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                                    <View style={{alignItems: 'flex-start', flex: 1}}>
                                        <Text style={{fontSize: 12}}>Valor a Transferir</Text>
                                    </View>
                                    <View style={{alignItems: 'flex-end', flex: 1}}>
                                        <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.amountToSend, { symbol: "AOA",  format: "%v %s" })}</Text>
                                    </View>
                            </View>

                            <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                                    <View style={{alignItems: 'flex-start', flex: 1}}>
                                        <Text style={{fontSize: 12}}>Cambio</Text>
                                    </View>
                                    <View style={{alignItems: 'flex-end', flex: 1}}>
                                        <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.foundRate, { symbol: "AOA",  format: "%v %s" })}</Text>
                                    </View>
                            </View>

                            <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                                    <View style={{alignItems: 'flex-start', flex: 1}}>
                                        <Text style={{fontSize: 12}}>Valor a Receber</Text>
                                    </View>
                                    <View style={{alignItems: 'flex-end', flex: 1}}>
                                        <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.amountToReceive, { symbol: this.state.currency,  format: "%v %s" })}</Text>
                                    </View>
                            </View>

                        </View>

                        <View style={{flex: 1, marginTop: 20, width: windowDimensions.width-100, alignSelf: 'center'}}>
                            <TouchableOpacity style={styles.buttonContainer} onPress={() => this.MakeTransfer()}>
                                <View style={{alignItems: 'flex-end', flex: 1}}>
                                    <Text style={styles.buttonText}>Transferir</Text>
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}> 
                                    <Spinner style={{alignSelf: 'center'}} isVisible={this.state.isSpinnerVisible} size={20} type={'FadingCircle'} color={'black'}/>
                                </View>
                            </TouchableOpacity>
                        </View>
                </ScrollView>
            </View>
        );
    }

    async GetRate(currParam, feePara) {
        var adtUrl = "?from=" + currParam
        try {
            let response = await fetch(config.rates.API + adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                var calcTotalFee = (this.state.amountToSend*feePara)/100;
                var calcTotalToSend = this.state.amountToSend + calcTotalFee;
                var calcAmountToReceive = this.state.amountToSend/responseJson.rate
                this.setState({
                    foundRate: responseJson.rate,
                    amountToReceive: calcAmountToReceive,
                    amountToSend: calcTotalToSend
                })
            }
            else {
                Alert.alert('ekumbu', responseJson.message);
            }
        } 
        catch(error) {
            Alert.alert('ekumbu', error.message);   
        }
    }

    async MakeTransfer() {
        
        this.setState({
            isSpinnerVisible: true
        })

        try {
            let response = await fetch(config.API.baseurl + "/api/v1/customer/sendTobank", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                },
                body: JSON.stringify({
                    sessiontoken : this.state.sessiontoken,
                    amountWFee: this.state.amountToSend,
                    originalAmount: this.state.originalAmount,
                    amountToTransfer : this.state.amountToReceive,
                    fee: this.state.fee,
                    rate: this.state.foundRate,
                    accountNumber : this.state.receiverBankAccount,
                    receiverName : this.state.receiverName,
                    country: this.state.country,
                    currency: this.state.currency,
                    institutionName: this.state.bank,
                    livemode: config.API.livemode,
                    description : this.state.description,
                    ipaddress : "0.0.0.0",

                })
            });
            
            let responseJson = await response.json();
            this.setState({
                isSpinnerVisible: false
            })

            if(responseJson.statusCode == 200) {
                try {
                    let transferInfo = {
                        total: this.state.amountToReceive,
                        currency: this.state.currency,
                        trackingID: responseJson.trackingID,
                        receiverName: this.state.receiverName
                    };
                    
                    AsyncStorage.mergeItem('TransferToAccount', JSON.stringify(transferInfo), () => {
                        this.props.navigator.replace({id: 'successtransfer'})
                    });
                } catch (error) {
                    Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
                }
                
            }
            else {
                Alert.alert('ekumbu', responseJson.message);
            }
        } 
        catch(error) {
            Alert.alert('ekumbu', error.message);   
        }
    }
}

module.exports = SubmitTransfer

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    amountToSendLayout: {
        alignItems: 'center',
        marginTop: 10,
        padding: 10,
        width: window.outerWidth,
        flex: 1,
        flexDirection: 'column'
    },
    buttonContainer : {
        backgroundColor: '#92918F',
        paddingVertical : 15,
        shadowColor: '#D1D1BA',
        shadowOffset: {
            width: 1,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 1.0,
        flexDirection: 'row'
    },
    SimpleButtonContainer: {
        backgroundColor: '#92918F',
        paddingVertical : 15,
        shadowColor: '#D1D1BA',
        shadowOffset: {
            width: 1,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 1.0,
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    modalCloseIcon: {
        width: 20,
        height: 20
    },
    flagIcon: {
        width: 25,
        height: 25,
    },
    banksIcon: {
        width: 40,
        height: 40,
    },
    separator: {
        height: 1,
        backgroundColor: 'black'
    },
    areaLayout: {
        width: windowDimensions.width - 50,
        alignSelf: 'center',
        flexDirection: 'column',
        padding: 10,
        marginTop: 3,
        borderRadius: 5, 
        borderWidth: 1,
        flex: 1,
        backgroundColor: '#f4f3ef',
        shadowColor: '#D1D1BA',
        shadowOffset: {
        width: 1,
        height: 1
      },
      shadowRadius: 1,
      shadowOpacity: 1.0
    },
    labelLayout: {
      width: windowDimensions.width - 50,
      alignSelf: 'center',
      flexDirection: 'column',
      padding: 10,
      marginTop: 10,
      flex: 1,
    },
    editButtonText: {
      textDecorationLine: "underline",
      textDecorationStyle: "solid",
      textDecorationColor: "#000",
      fontSize: 9
    },
    input: {
        height: 40,
        width: windowDimensions.width-30,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        marginBottom: 10,
        paddingHorizontal : 10
    },
}