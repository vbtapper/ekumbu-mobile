import React, { Component } from 'react';
import { AppRegistry, Text, View, ScrollView, TouchableOpacity, Dimensions, AsyncStorage, Alert, Modal, Image, TextInput } from 'react-native';

import ServiceMenu from "../ServiceMenu"

const windowDimensions              = Dimensions.get('window');
const config                        = require("../../../utils/Configuration/config");
const EkumbuNumericalPad            = require('../../../utils/KeyboardPad/NumericalPad');
const currencyFormatter             = require('currency-formatter');
const Spinner                       = require('react-native-spinkit');

function TruckString(value, n) {
    return (value.length > n) ? value.substr(0, n-1) + '...' : value;
}

class SubmitSending extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sessiontoken: '',
            backroute: 'entersendingamount',
            countryIndex: 0,
            countryFee: 0,
            country: 'Angola',
            receiverCard: '',
            receiverCardLast4: '',
            descriptionModal: '',
            description: '',
            receiverCardNumberModal: '',
            currency: 'AOA',
            amountToSend: 0,
            amountToReceive: 0,
            showDescriptionModal : false,
            receverInfoModal: false,
            isSpinnerVisible: false
        }  
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem('SendingToEkumbu', (err, result) => {
                var sendingObj = JSON.parse(result);
                this.setState({amountToSend: sendingObj.amountToSend, amountToReceive: sendingObj.amountToSend })
            });
            AsyncStorage.getItem('LoggedCustomer', (err, result) => {
                var LoggedCustomer = JSON.parse(result);
                this.setState({sessiontoken: LoggedCustomer.sessionToken})
            });
        }
        catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 1");
        }
    }

    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    ShowReceiverInfo() {
        this.setState({
            receverInfoModal: true
        })
    }

    HideReceiverInfo() {
        this.setState({
            receverInfoModal: false
        })
    }

    ShowEditDescription() {
        this.setState({
            showDescriptionModal: true
        })
    }

    HideEditDescription() {
        this.setState({
            showDescriptionModal: false
        })
    }

    SaveReceiverInfo() {
        this.setState({
            receiverCard: this.state.receiverCardNumberModal,
            receiverCardLast4: "x-" + this.state.receiverCardNumberModal.substr(this.state.receiverCardNumberModal.length-4, this.state.receiverCardNumberModal.length),
            receverInfoModal: false
        })
    }

    SaveDescriptions() {
        this.setState({
            description: this.state.descriptionModal,
            showDescriptionModal: false
        })
    }
    
    render() {
        return (
            <View style={styles.page}>
                <View>
                    <ServiceMenu onPress={() => this.BackToMain()}/>
                </View>
                <ScrollView style={{flex: 1, flexDirection: 'column'}}>
                    <View style={styles.labelLayout}>
                        <Text style={{fontSize: 9}}>Informações do Receptor</Text>
                    </View>
                    <View style={styles.areaLayout}>
                        <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{alignItems: 'flex-start', width: 60}}>
                                    <Text style={{fontSize: 12}}>Cartão</Text>
                                </View>
                                <View style={{alignItems: 'flex-start', flex: 1}}>
                                    <Text style={{fontSize: 13, marginLeft: 5}}>{this.state.receiverCardLast4}</Text>
                                </View>
                                <View style={{alignItems: 'flex-end', width:26}}>
                                    <TouchableOpacity style={{alignSelf: 'center'}} onPress={() => this.ShowReceiverInfo()}>
                                        <Text style={styles.editButtonText}>editar</Text>
                                    </TouchableOpacity>
                                </View>
                                <Modal
                                    animationType={"slide"}
                                    transparent={true}
                                    visible={this.state.receverInfoModal}>
                                    <View>
                                        <ScrollView style={{marginTop: 60, width: windowDimensions.width-30, height: windowDimensions.height-50, backgroundColor: '#f4f3ef', alignSelf: 'center', flexDirection: 'column', borderWidth: 0, borderColor: 'black'}}>
                                            <View style={{flexDirection: 'row', padding: 10, flex: 1, }}>
                                                <View style={{alignItems: 'flex-start'}}>
                                                    <TouchableOpacity onPress={() => this.HideReceiverInfo()}>
                                                        <Image source={require('../../../../images/xclose.png')} style={styles.modalCloseIcon}  />
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{alignItems: 'center', flex: 1}}>
                                                    <Text style={{fontSize: 12, alignSelf: 'center'}}>Informações do Receptor</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection: 'column', flex: 1, padding: 10}}>
                                                <View>
                                                    <TextInput 
                                                        placeholder='Numero do Cartão Ekumbu'
                                                        returnKeyType="next"
                                                        keyboardType="number-pad"
                                                        autoCapitalize="none"
                                                        onChangeText={ (text)=> this.setState({receiverCardNumberModal: text}) }
                                                        autoCorrect={false}
                                                        style={styles.input}>
                                                    </TextInput>
                                                </View>
                                                <View style={{marginTop: 5, flex: 1, padding: 10, width: windowDimensions.width-100, alignSelf: 'center'}}>
                                                    <TouchableOpacity style={styles.SimpleButtonContainer} onPress={() => this.SaveReceiverInfo()}>
                                                        <Text style={styles.buttonText}>Ok</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </ScrollView>
                                    </View>
                                </Modal>
                            </View>
                        </View>

                        <View style={styles.labelLayout}>
                            <Text style={{fontSize: 9}}>Descrição do Envio</Text>
                        </View>

                        <View style={styles.areaLayout}>
                        <View style={{flexDirection: 'row', flex: 1}}>
                                <View style={{alignItems: 'flex-start', width: 60}}>
                                    <Text style={{fontSize: 12}}>Descrição</Text>
                                </View>
                                <View style={{alignItems: 'flex-start', flex: 1}}>
                                    <Text style={{fontSize: 13, marginLeft: 5}}>{this.state.description}</Text>
                                </View>
                                <View style={{alignItems: 'flex-end', width:26}}>
                                    <TouchableOpacity style={{alignSelf: 'center'}} onPress={() => this.ShowEditDescription()}>
                                        <Text style={styles.editButtonText}>editar</Text>
                                    </TouchableOpacity>
                                </View>
                                <Modal
                                    animationType={"slide"}
                                    transparent={true}
                                    visible={this.state.showDescriptionModal}>
                                    <View>
                                        <ScrollView style={{marginTop: 60, width: windowDimensions.width-30, height: windowDimensions.height-50, backgroundColor: '#f4f3ef', alignSelf: 'center', flexDirection: 'column', borderWidth: 0, borderColor: 'black'}}>
                                            <View style={{flexDirection: 'row', padding: 10, flex: 1, }}>
                                                <View style={{alignItems: 'flex-start'}}>
                                                    <TouchableOpacity onPress={() => this.HideEditDescription()}>
                                                        <Image source={require('../../../../images/xclose.png')} style={styles.modalCloseIcon}  />
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{alignItems: 'center', flex: 1}}>
                                                    <Text style={{fontSize: 12, alignSelf: 'center'}}>Descrição do Envio</Text>
                                                </View>
                                            </View>
                                            <View style={{flexDirection: 'column', flex: 1, padding: 10}}>
                                                <View>
                                                    <TextInput 
                                                        placeholder='Nome'
                                                        returnKeyType="next"
                                                        keyboardType="ascii-capable"
                                                        autoCapitalize="none"
                                                        onChangeText={ (text)=> this.setState({descriptionModal: text}) }
                                                        autoCorrect={false}
                                                        style={styles.input}>
                                                    </TextInput>
                                                </View>
                                                <View style={{marginTop: 5, flex: 1, padding: 10, width: windowDimensions.width-100, alignSelf: 'center'}}>
                                                    <TouchableOpacity style={styles.SimpleButtonContainer} onPress={() => this.SaveDescriptions()}>
                                                        <Text style={styles.buttonText}>Ok</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </ScrollView>
                                    </View>
                                </Modal>
                            </View>
                        </View>

                        <View style={styles.labelLayout}>
                            <Text style={{fontSize: 9}}>Valores do Transferencia</Text>
                        </View>

                        <View style={styles.areaLayout}>

                            <View style={{flexDirection: 'row', flex: 1}}>
                                    <View style={{alignItems: 'flex-start', flex: 1}}>
                                        <Text style={{fontSize: 12}}>Valor a Enviar</Text>
                                    </View>
                                    <View style={{alignItems: 'flex-end', flex: 1}}>
                                        <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.amountToSend, { symbol: "AOA",  format: "%v %s" })}</Text>
                                    </View>
                            </View>

                            <View style={{flexDirection: 'row', flex: 1, marginTop: 10}}>
                                    <View style={{alignItems: 'flex-start', flex: 1}}>
                                        <Text style={{fontSize: 12}}>Valor a Receber</Text>
                                    </View>
                                    <View style={{alignItems: 'flex-end', flex: 1}}>
                                        <Text style={{fontSize: 13}}>{currencyFormatter.format(this.state.amountToReceive, { symbol: this.state.currency,  format: "%v %s" })}</Text>
                                    </View>
                            </View>

                        </View>

                        <View style={{flex: 1, marginTop: 20, width: windowDimensions.width-100, alignSelf: 'center'}}>
                            <TouchableOpacity style={styles.buttonContainer} onPress={() => this.MakeTransfer()}>
                                <View style={{alignItems: 'flex-end', flex: 1}}>
                                    <Text style={styles.buttonText}>Enviar</Text>
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}> 
                                    <Spinner style={{alignSelf: 'center'}} isVisible={this.state.isSpinnerVisible} size={20} type={'FadingCircle'} color={'black'}/>
                                </View>
                            </TouchableOpacity>
                        </View>

                </ScrollView>
            </View>
        );
    }

    async MakeTransfer() {
        
        this.setState({
            isSpinnerVisible: true
        })

        try {
            let response = await fetch(config.sendmoney.url + "/sendmoney", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.sendmoney.access_key
                },
                body: JSON.stringify({
                    ipaddress : '0.0.0.0',
                    description : this.state.description,
                    dstCardN : this.state.receiverCard,
                    sessiontoken : this.state.sessiontoken,
                    livemode: config.API.livemode,
                    amount: this.state.amountToSend
                })
            });
            
            let responseJson = await response.json();
            this.setState({
                isSpinnerVisible: false
            })

            if(responseJson.statusCode == 200) {
                try {
                    let sendInfo = {
                        total: this.state.amountToReceive,
                        trackingID: responseJson.trackingID,
                        cardLast4: this.state.receiverCardLast4
                    };
                    
                    AsyncStorage.mergeItem('SendingToEkumbu', JSON.stringify(sendInfo), () => {
                        this.props.navigator.replace({id: 'successsending'})
                    });
                } catch (error) {
                    Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
                }
                
            }
            else {
                Alert.alert('ekumbu', responseJson.message);
            }
        } 
        catch(error) {
            Alert.alert('ekumbu', error.message);   
        }
    }
}

module.exports = SubmitSending

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    amountToSendLayout: {
        alignItems: 'center',
        marginTop: 10,
        padding: 10,
        width: window.outerWidth,
        flex: 1,
        flexDirection: 'column'
    },
    buttonContainer : {
        backgroundColor: '#92918F',
        paddingVertical : 15,
        shadowColor: '#D1D1BA',
        shadowOffset: {
            width: 1,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 1.0,
        flexDirection: 'row'
    },
    SimpleButtonContainer: {
        backgroundColor: '#92918F',
        paddingVertical : 15,
        shadowColor: '#D1D1BA',
        shadowOffset: {
            width: 1,
            height: 1
        },
        shadowRadius: 1,
        shadowOpacity: 1.0,
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    modalCloseIcon: {
        width: 20,
        height: 20
    },
    flagIcon: {
        width: 25,
        height: 25,
    },
    banksIcon: {
        width: 40,
        height: 40,
    },
    separator: {
        height: 1,
        backgroundColor: 'black'
    },
    areaLayout: {
        width: windowDimensions.width - 50,
        alignSelf: 'center',
        flexDirection: 'column',
        padding: 10,
        marginTop: 3,
        borderRadius: 5, 
        borderWidth: 1,
        flex: 1,
        backgroundColor: '#f4f3ef',
        shadowColor: '#D1D1BA',
        shadowOffset: {
        width: 1,
        height: 1
      },
      shadowRadius: 1,
      shadowOpacity: 1.0
    },
    labelLayout: {
      width: windowDimensions.width - 50,
      alignSelf: 'center',
      flexDirection: 'column',
      padding: 10,
      marginTop: 10,
      flex: 1,
    },
    editButtonText: {
      textDecorationLine: "underline",
      textDecorationStyle: "solid",
      textDecorationColor: "#000",
      fontSize: 9
    },
    input: {
        height: 40,
        width: windowDimensions.width-30,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        marginBottom: 10,
        paddingHorizontal : 10
    },
}