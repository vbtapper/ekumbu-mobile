import React, { Component } from 'react';
import { AppRegistry, Text, View, ScrollView, TouchableOpacity, Dimensions, AsyncStorage, Alert } from 'react-native';

import ServiceMenu from "../ServiceMenu"

const windowDimensions = Dimensions.get('window');
const config = require("../../../utils/Configuration/config");
const EkumbuNumericalPad = require('../../../utils/KeyboardPad/NumericalPad');
const currencyFormatter = require('currency-formatter');

let model = {
    
    _keys: [],

    _listeners: [],

    addKey(key) {
        this._keys.push(key);
        this._notify();
    },

    delKey() {
        this._keys.pop();
        this._notify();
    },

    clearAll() {
        this._keys = [];
        this._notify();
    },

    getKeys() {
        return this._keys;
    },

    onChange(listener) {
        if (typeof listener === 'function') {
            this._listeners.push(listener);
        }
    },

    _notify() {
        this._listeners.forEach((listner) => {
            listner(this);
        });
    }
};

class EnterSendingAmount extends Component {

    constructor(props) {
        super(props);

        this.state = {
            backroute: 'chooseservice',
            amountToSend: 0,
        }  
    }

    componentDidMount() {
        model.clearAll();
    }

    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    _handleClear() {
        model.clearAll();
    }
 
    _handleDelete() {
        model.delKey();
    }
 
    _handleKeyPress(key) {
        model.addKey(key);
    }

    render() {
        
        model.onChange((model) => {
            this.setState(
                {
                    amountToSend: model.getKeys().join(''),
                }
            );
        });

        return (
            <View style={styles.page}>
                <View>
                    <ServiceMenu onPress={() => this.BackToMain()}/>
                </View>
                <ScrollView style={{flex: 1, flexDirection: 'column'}}>

                    <View style={styles.amountToSendLayout}>
                        <View style={{alignItems: 'flex-start', flex: 1, justifyContent: 'center', flexDirection: 'row'}}>
                            <Text style={{fontSize: 36, alignSelf: 'flex-end'}}>
                                {currencyFormatter.format(this.state.amountToSend, { code: 'AOA' })}
                            </Text>
                        </View>
                    </View>

                    <View style={{flex: 1, width: windowDimensions.width-100, marginTop: 50, alignSelf: 'center'}}>
                        <EkumbuNumericalPad 
                            keyboardType="decimal-pad"
                            onClear={this._handleClear.bind(this)}
                            onDelete={this._handleDelete.bind(this)}
                            onKeyPress={this._handleKeyPress.bind(this)}
                        />
                    </View>

                    <View style={{flex: 1, marginTop: 30, width: windowDimensions.width-100, alignSelf: 'center'}}>
                        <TouchableOpacity style={styles.buttonContainer} onPress={() => this.cmdNextAction()}>
                            <Text style={styles.buttonText}>Seguinte</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </View>
        );
    }

    cmdNextAction() {
        if(parseInt(this.state.amountToSend) > 0) {
            try {
                let sendingObj = {
                    amountToSend: parseInt(this.state.amountToSend),
                };

                AsyncStorage.setItem('SendingToEkumbu', JSON.stringify(sendingObj), () => {
                    this.props.navigator.replace({id: 'submitsending'})
                });
                
            } catch (error) {
                Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
            }
        }
    }
}

module.exports = EnterSendingAmount

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    amountToSendLayout: {
        alignItems: 'center',
        marginTop: 10,
        padding: 10,
        width: window.outerWidth,
        flex: 1,
        flexDirection: 'column'
    },
    buttonContainer : {
        backgroundColor: '#92918F',
        paddingVertical : 15
    },
    buttonText : {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight : '700'
    },
    modalCloseIcon: {
        width: 20,
        height: 20
    },
    currencyIcon: {
        width: 25,
        height: 25,
    },
    separator: {
        height: 1,
        backgroundColor: 'black'
    },
}