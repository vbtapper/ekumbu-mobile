import React, { Component } from 'react';
import { AppRegistry, Text, View, ScrollView, Dimensions, TouchableOpacity, Image } from 'react-native';

import ServiceMenu from "./ServiceMenu"
const windowDimensions = Dimensions.get('window');

class ChooseService extends Component {

    constructor(props) {
        super(props);

        this.state = {
            backroute: 'first'
        }     
    }

    componentDidMount() {
        
    }

    BackToMain() {
        this.props.navigator.replace({id: this.state.backroute})
    }

    GoToDeposits() {
        this.props.navigator.replace({id: 'choosedepositservice' })
    }

    GoToTransfers() {
        this.props.navigator.replace({id: 'entertransferamount' })
    }

    GoToSending() {
        this.props.navigator.replace({id: 'entersendingamount' })
    }

    render() {
        return (
            <View style={styles.page}>
                <View style={styles.serviceMenu}>
                    <ServiceMenu onPress={() => this.BackToMain()}/>
                </View>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={styles.MainLayout}>
                        <View style={{alignItems: 'center', marginBottom: 25}}>
                            <Text style={{fontSize: 10, fontWeight: 'bold'}}>SELECIONE O TIPO DE SERVIÇO</Text>
                        </View>
                        <View style={styles.optionsLayout}>
                            <View style={{marginRight: 10}}>
                                <TouchableOpacity style={styles.optionsButtons} onPress={() => this.GoToDeposits()} >
                                    <Image source={require('../../../images/deposit.png')} style={styles.optionLogo} />
                                    <Text style={{alignSelf: 'center', fontSize: 7, marginTop: 5, fontWeight: 'bold', }}>DEPOSITO</Text>
                                </TouchableOpacity>    
                            </View>
                            <View style={{marginRight: 10}}>
                                <TouchableOpacity style={styles.optionsButtons} onPress={() => this.GoToTransfers()}>
                                    <Image source={require('../../../images/transfer.png')} style={styles.optionLogo} />
                                    <Text style={{alignSelf: 'center', fontSize: 7, marginTop: 5, fontWeight: 'bold', }}>TRANSFÊRENCIA</Text>
                                </TouchableOpacity>  
                            </View>
                            <View style={{marginRight: 10}}>
                                <TouchableOpacity style={styles.optionsButtons} onPress={() => this.GoToSending()}>
                                    <Image source={require('../../../images/sendm.png')} style={styles.optionLogo} />
                                    <Text style={{alignSelf: 'center', fontSize: 7, marginTop: 5, fontWeight: 'bold', }}>ENVIO</Text>
                                </TouchableOpacity>  
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

module.exports = ChooseService

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    serviceMenu: {
        width: window.width,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    MainLayout: {
        flexDirection: 'column',
        justifyContent: 'center',
        width: windowDimensions.width,
        height: windowDimensions.height,
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
    },
    optionsButtons: {
        width: 80,
        height: 80,
        paddingBottom: 2,
        justifyContent: 'center',
        borderColor: '#808080',
        borderWidth: 1,
        borderRadius: 50,
        flexDirection: 'column'
    },
    optionLogo: {
        width: 30,
        height: 30,
        alignSelf: 'center'
    },
    optionsLayout: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        width: window.innerWidth,
        padding: 10
    }
}
