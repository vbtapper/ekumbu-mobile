import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, KeyboardAvoidingView, Navigator, Image, Dimensions, Alert, AsyncStorage } from 'react-native';

const SideMenu = require('react-native-side-menu');

const config = require("../utils/Configuration/config");

import MenuButton from "../utils/SideMenu/MenuButton"
import Menu from "../utils/SideMenu/Menu"
import DetailMenu from "./DetailMenu"

import General from "./General"
import Payment from "./Payments"

import DepositTransaction from './TransactionsType/DepositTransaction'
import TransactionDetails from './TransactionDetails'
import AllTransactions from './AllTransactions'
import TransactionsOptions from './AllTransactionsDetailsOptions'
import ChooseService from './Services/ChooseService'
import ChooseDepositType from './Services/Deposits/ChooseDepositType'

//Deposits
import KioskyDeposit from './Services/Deposits/KiosKy/KioskyDeposit'
import KioskyDepositDetail from './Services/Deposits/KiosKy/KioskyDepositDetail'
import EnterCrediCardDeposit from './Services/Deposits/CreditCard/EnterDepositDetails'
import SubmitCreditCardDeposit from './Services/Deposits/CreditCard/SubmitCreditCardDeposit'
import SuccessCreditCardDeposit from './Services/Deposits/CreditCard/SuccessCreditCardDeposit'

//Transfers
import EnterTransferAmount from './Services/Transfers/EnterTransferAmount'
import SubmitTransfer from './Services/Transfers/SubmitTransfer'
import SuccessTransfer from './Services/Transfers/SuccessTransfer'

//Sending
import EnterSendingAmount from './Services/Sending/EnterSendingAmount'
import SubmitSending from './Services/Sending/SubmitSending'
import SuccessSending from './Services/Sending/SuccessSend'

const window = Dimensions.get('window');

class FirstPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
      return (
            <General navigator={this.props.navigator}/>
      );    
    }
}

class SecondPage extends Component {
    render() {
        return (
            <Payment />
        );    
    }
}

class FirstPageMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    toggle() {
        this.setState({
          isOpen: !this.state.isOpen,
        });
    }

    updateMenuState(isOpen) {
        this.setState({ isOpen, });
    }

    onMenuItemSelected = (item) => {
        this.setState({
            isOpen: false,      
            selectedItem: item,
        });
        this.props.navigator.replace({ id: item });
    }

    chooseServie() {
        this.props.navigator.replace({ id: 'chooseservice' });
    }

    render() {

        const menu = <Menu onItemSelected={this.onMenuItemSelected} navigator={this.props.navigator}/>;

        return (
            <SideMenu menu={menu} isOpen={this.state.isOpen} onChange={(isOpen) => this.updateMenuState(isOpen)}>
                <MenuButton onPress={() => this.toggle()} addNewService={() => this.chooseServie()}/>
                <FirstPage navigator={this.props.navigator}/>
            </SideMenu>
        );
    }
};

class SecondPageMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    toggle() {
        this.setState({
          isOpen: !this.state.isOpen,
        });
    }

    updateMenuState(isOpen) {
        this.setState({ isOpen, });
    }

    onMenuItemSelected = (item) => {
        this.setState({
            isOpen: false,      
            selectedItem: item,
        });
        this.props.navigator.replace({ id: item });
    }

    render() {

        const menu = <Menu onItemSelected={this.onMenuItemSelected} navigator={this.props.navigator}/>;

        return (
            <SideMenu
              menu={menu}
              isOpen={this.state.isOpen}
              onChange={(isOpen) => this.updateMenuState(isOpen)}>
                <MenuButton onPress={() => this.toggle()}/>
                <SecondPage/>
            </SideMenu>                
        );
    }    
}

class ThirdPage extends Component {
     
}

class ThirdPageMenu extends Component {
     
}

export default class MainDashboard extends Component {    

    constructor(props) {
        super(props);
        this._setNavigatorRef = this._setNavigatorRef.bind(this);
    }

    renderScene(route, nav) {
        switch (route.id) {
        case 'first':
            return <FirstPageMenu navigator={nav} />;
        case 'second':
            return <SecondPageMenu navigator={nav} />;
        case 'third':
            return <ThirdPageMenu navigator={nav} />;
        case 'transdetails':
            return <TransactionDetails navigator={nav} />
        case 'alltransactions':
            return <AllTransactions navigator={nav}/>
        case 'transactionsoptions':
            return <TransactionsOptions navigator={nav}/>
        case 'chooseservice':
            return <ChooseService navigator={nav}/>
        case 'choosedepositservice':
            return <ChooseDepositType navigator={nav}/>
        case 'kioskydeposit':
            return <KioskyDeposit navigator={nav}/>
        case 'kioskydepositdetails':
            return <KioskyDepositDetail navigator={nav}/>
        case 'enterdetailsccdeposit':
            return <EnterCrediCardDeposit navigator={nav}/>
        case 'submitccdeposit':
            return <SubmitCreditCardDeposit navigator={nav}/>
        case 'successccdeposit':
            return <SuccessCreditCardDeposit navigator={nav}/>
        case 'entertransferamount':
            return <EnterTransferAmount navigator={nav}/>
        case 'submittransfer':
            return <SubmitTransfer navigator={nav}/>
        case 'successtransfer':
            return <SuccessTransfer navigator={nav}/>
        case 'entersendingamount':
            return <EnterSendingAmount navigator={nav}/>
        case 'submitsending':
            return <SubmitSending navigator={nav}/>
        case 'successsending':
            return <SuccessSending navigator={nav}/>
        default:
            return <FirstPageMenu navigator={nav} />;
        }
    }

    render() {
        return (
            <Navigator
                ref={this._setNavigatorRef}
                initialRoute={{id: 'first'}}
                renderScene={this.renderScene}
                configureScene={(route) => {
                if (route.sceneConfig) {
                    return route.sceneConfig;
                }
                return Navigator.SceneConfigs.FloatFromBottom;
                }}
            />
        );
    }

  componentWillUnmount() {
    this._listeners && this._listeners.forEach(listener => listener.remove());
  }

  _setNavigatorRef(navigator) {
    if (navigator !== this._navigator) {
      this._navigator = navigator;

      if (navigator) {
        var callback = (event) => {
          /*console.log(
             'NavigatorMenu: event ${event.type}',
            {
              route: JSON.stringify(event.data.route),
              target: event.target,
              type: event.type,
            }
          );*/
        };
        // Observe focus change events from the owner.
        this._listeners = [
          navigator.navigationContext.addListener('willfocus', callback),
          navigator.navigationContext.addListener('didfocus', callback),
        ];
      }
    }
  }
}

const styles = {
    container: {
        flex : 1,
        backgroundColor: '#f4f3ef'
    },
    menuComponent: {
        flex: 1, 
        backgroundColor: '#f4f3ef',
        paddingTop: 50
    },
    menu: {
        flex: 1,
        width: window.width,
        height: window.height,
        padding: 20,
    },
    item: {
      fontSize: 16,
      fontWeight: '300',
      paddingTop: 20,
    },    
    page: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#f4f3ef'
    },
    pageContent: {
        flex: 1,
        alignItems: 'center',
        top: 200,
    },
    detailMenu: {
        width: window.width,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
}
