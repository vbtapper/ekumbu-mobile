import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator } from 'react-native';

import General from "./General"

export default class Payment extends Component {    

    render() {
        return (
            <View style={styles.page}>
                <Text style={styles.pageContent}>Payment</Text>
            </View>
        );
    }
}

const styles = {
    page: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#f4f3ef'
    },
    pageContent: {
        flex: 1,
        alignItems: 'center',
        top: 200,
    },
}