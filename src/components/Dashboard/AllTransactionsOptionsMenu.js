import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator, Image } from 'react-native';

class AllTransactionsOptionsMenu extends Component {

  handlePressBack(e) {
    if (this.props.handlePressBack) {
      this.props.handlePressBack(e);
    }
  }

  handlePressSave(e) {
    if (this.props.handlePressSave) {
      this.props.handlePressSave(e);
    }
  }

  render() {
    return (
      <View style={styles.menuButton} >
        <View style={{flex: 1, alignItems: 'flex-start'}}>
            <TouchableOpacity onPress={this.handlePressBack.bind(this)}>
                <Image style={styles.btIcon} source={require('../../images/back.png')}/>        
            </TouchableOpacity>  
        </View>
        <View style={{flex: 1, alignItems: 'flex-end', justifyContent: 'center', alignSelf: 'center'}}>
            <TouchableOpacity onPress={this.handlePressSave.bind(this)}>
                <Image style={styles.calendarIcon} source={require('../../images/save.png')}/>        
            </TouchableOpacity>  
        </View>
      </View>
    );
  }
}

const styles = {
    menuButton: {
        flexDirection: 'row',
        marginTop: 20,
        backgroundColor: '#D1D1BA',
        height: 40
    },
    exitButton: {
        marginTop: 25,
        marginRight: 5
    },
    exitButtonArea: {
        alignItems: 'flex-end',
        flex: 1
    },
    exitText: {
        fontSize: 16,
        fontWeight: '600'
    },
    btIcon: {
        width: 30,
        height: 30,
        marginTop: 5
    },
    calendarIcon: {
        width: 24,
        height: 24,
        marginRight: 3
    }  
}

module.exports = AllTransactionsOptionsMenu