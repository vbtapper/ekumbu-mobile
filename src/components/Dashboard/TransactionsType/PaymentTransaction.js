import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator, Image, Alert, AsyncStorage } from 'react-native';

import DetailMenu from "../DetailMenu"
const config = require("../../utils/Configuration/config");

class PaymentTransaction extends Component {

  handlePress(e) {
    if (this.props.onPress) {
      this.props.onPress(e);
    }
  }

  constructor(props) {
        super(props);

        this.state = {
            amountCharged: '',
            source: '',
            trackingID: '',
            status: '',
            date: '',
            backroute: ''
        }     
    }

  componentDidMount() {
        try {
            AsyncStorage.getItem('TransactionDetails', (err, result) => {
                var transactionDetails = JSON.parse(result);
                this.setState({detailsID: transactionDetails.id, backroute: transactionDetails.backroute})
            });

            AsyncStorage.getItem('LoggedCustomer', (err, result) => {
                var LoggedCustomer = JSON.parse(result);
                this.setState({sessionToken: LoggedCustomer.sessionToken})
                this.GetTransactionDetail();
            });
                
        }
        catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 1");
        }
    }

  BackToMain() {
    this.props.navigator.replace({id: this.state.backroute})
  }

  render() {
    return (
      <View style={styles.page}>
            <View style={styles.detailMenu}>
                <DetailMenu onPress={() => this.BackToMain()}/>
            </View>
            <ScrollView>
                <View style={styles.transDetailsBanner}>
                    <Image source={require('../../../images/paymentDetails.png')} style={styles.transDetailsBannerIcon} />
                    <Text style={styles.transDetailsBannerLineOne}>Pagamento para efectuado {this.state.source}</Text>
                    <Text style={styles.transDetailsBannerLineTwo}>foi retirado {this.state.amountCharged} do o seu cartão ekumbu</Text>
                </View>
                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Para</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.source}</Text>
                    </View>
                </View>
                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Total</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.amountCharged}</Text>
                    </View>
                </View>
                <View style={styles.separator}/>

                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Código de Rastreamento</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.trackingID}</Text>
                    </View>
                </View>
                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Estado</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.status}</Text>
                    </View>
                </View>
                <View style={styles.separator}/>
                <View style={styles.transDetailsFrom}>
                    <View>
                        <Text style={styles.transDetailsValue}>{this.state.date}</Text>
                    </View>
                </View>
            </ScrollView>   
        </View> 
    );
  }

  async GetTransactionDetail() {
        var adtUrl = "/api/v1/customer/gevtdetail?" + "sessiontoken=" + this.state.sessionToken + "&livemode=" + config.API.livemode + "&srcID=" + this.state.detailsID
        try {
            let response = await fetch(config.API.baseurl +  adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                },
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                
                this.setState({
                    source: responseJson.merchantInfo.name,
                    amountCharged: responseJson.total,
                    trackingID: responseJson.trackingID,
                    status: responseJson.status,
                    date: responseJson.dateProcess
                })
            }
            else {
                Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 1');
            }
        } 
        catch(error) {
            Alert.alert(error.message)
            Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 2');   
        }
    }  
}

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    transDetailsBanner: {
        flex: 0,
        width: window.width,
        height: 200,
        backgroundColor: '#E6E6D9',
        alignItems : 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        opacity: 0.9
    },
    transDetailsBannerLineTwo: {
        fontSize: 9,
        marginTop: 5,
        color: 'black',
        fontWeight: 'bold'
    },
    transDetailsBannerLineOne: {
        fontSize: 11,
        color: 'black',
        marginTop: 8,
        fontWeight: 'bold'
    },
    transDetailsBannerIcon: {
        height: 60,
        width: 60,       
    },
    detailMenu: {
        width: window.width,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    transDetailsFrom: {
        padding: 10,
        flexDirection: 'row',
        width: window.width,
        justifyContent: 'space-around',
    },
    separator: {
        height: 1,
        backgroundColor: '#dddddd'
    },
    transDetailsFromColumnLabel: {
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignItems: 'flex-start',
        flex: 1
    },

    transDetailsFromColumnValue: {
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flex: 1
    },

    transDetailsValue: {
        fontSize: 12
    }
}

module.exports = PaymentTransaction