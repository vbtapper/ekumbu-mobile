import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator, Image, Alert, AsyncStorage } from 'react-native';

import DetailMenu from "../DetailMenu"
const config = require("../../utils/Configuration/config");

class DepositTransaction extends Component {

    constructor(props) {
        super(props);

        this.state = {
            amountCharged: '',
            amountReceived: '',
            date: '',
            location: '',
            fee: '',
            status: '',
            trackingID: '',
            rate: '',
            backroute: ''
        }     
    }

  componentDidMount() {
        try {
            AsyncStorage.getItem('TransactionDetails', (err, result) => {
                var transactionDetails = JSON.parse(result);
                this.setState({detailsID: transactionDetails.id, backroute: transactionDetails.backroute})
            });

            AsyncStorage.getItem('LoggedCustomer', (err, result) => {
                var LoggedCustomer = JSON.parse(result);
                this.setState({sessionToken: LoggedCustomer.sessionToken})
                this.GetTransactionDetail();
            });
                
        }
        catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 1");
        }
    }

  BackToMain() {
    this.props.navigator.replace({id: this.state.backroute})
  }

  render() {
    return (
      <View style={styles.page}>
            <View style={styles.detailMenu}>
                <DetailMenu onPress={() => this.BackToMain()}/>
            </View>
            <ScrollView>
                <View style={styles.transDetailsBanner}>
                    <Image source={require('../../../images/depositDetails.png')} style={styles.transDetailsBannerIcon} />
                    <Text style={styles.transDetailsBannerLineOne}>Deposito de {this.state.amountCharged} feito a partir de x-{this.state.location}   </Text>
                    <Text style={styles.transDetailsBannerLineTwo}>foi depositado {this.state.amountReceived} para o seu cartão ekumbu</Text>
                </View>
                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Total Depositado</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.amountCharged}</Text>
                    </View>
                </View>
                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Taxa Paga</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.fee}</Text>
                    </View>
                </View>
                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Cambio</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.rate}</Text>
                    </View>
                </View>
                <View style={styles.separator}/>

                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Total Recebido</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.amountReceived}</Text>
                    </View>
                </View>
                <View style={styles.separator}/>

                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Código de Rastreamento</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.trackingID}</Text>
                    </View>
                </View>
                <View style={styles.transDetailsFrom}>
                    <View style={styles.transDetailsFromColumnLabel}>
                        <Text style={styles.transDetailsValue}>Estado</Text>
                    </View>
                    <View style={styles.transDetailsFromColumnValue}>
                        <Text style={styles.transDetailsValue}>{this.state.status}</Text>
                    </View>
                </View>
                <View style={styles.separator}/>
                <View style={styles.transDetailsFrom}>
                    <View>
                        <Text style={styles.transDetailsValue}>{this.state.date}</Text>
                    </View>
                </View>
            </ScrollView>   
        </View> 
    );
  }

  async GetTransactionDetail() {
        var adtUrl = "/api/v1/customer/gevtdetail?" + "sessiontoken=" + this.state.sessionToken + "&livemode=" + config.API.livemode + "&srcID=" + this.state.detailsID
        try {
            let response = await fetch(config.API.baseurl +  adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                },
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                this.setState({
                    amountCharged: responseJson.amountCharged,
                    amountReceived: responseJson.amountReceived,
                    date: responseJson.dateProcess,
                    location: responseJson.sourceInfo.last4,
                    fee: responseJson.fee,
                    status: responseJson.status,
                    trackingID: responseJson.trackingID,
                    rate: responseJson.rate,
                })
            }
            else {
                Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 1');
            }
        } 
        catch(error) {
            Alert.alert(error.message)
            Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 2');   
        }
    }  
}

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    transDetailsBanner: {
        flex: 0,
        width: window.width,
        height: 200,
        backgroundColor: '#E6E6D9',
        alignItems : 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        opacity: 0.9
    },
    transDetailsBannerLineTwo: {
        fontSize: 9,
        marginTop: 5,
        color: 'black',
        fontWeight: 'bold'
    },
    transDetailsBannerLineOne: {
        fontSize: 11,
        color: 'black',
        marginTop: 8,
        fontWeight: 'bold'
    },
    transDetailsBannerIcon: {
        height: 60,
        width: 60,       
    },
    transDetailsFrom: {
        padding: 10,
        flexDirection: 'row',
        width: window.width,
        justifyContent: 'space-around',
    },
    detailMenu: {
        width: window.width,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    transDetailsFromColumnLabel: {
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignItems: 'flex-start',
        flex: 1
    },

    transDetailsFromColumnValue: {
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flex: 1
    },
    separator: {
        height: 1,
        backgroundColor: '#dddddd'
    },

    transDetailsValue: {
        fontSize: 12
    }
}

module.exports = DepositTransaction