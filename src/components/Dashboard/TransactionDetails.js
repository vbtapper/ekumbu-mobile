import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, KeyboardAvoidingView, Navigator, Image, Dimensions, Alert, AsyncStorage } from 'react-native';

import DepositTransaction from './TransactionsType/DepositTransaction'
import PaymentTransaction from './TransactionsType/PaymentTransaction'
import TransferTransaction from './TransactionsType/TransferTransaction'
import SendTransaction from './TransactionsType/SendTransaction'

import renderIf from './renderIf';

class TransactionDetails extends Component {

    constructor(props) {
        super(props);

        this.state = {
            type: ''
        }     

         AsyncStorage.getItem('TransactionDetails', (err, result) => {
            var transactionDetails = JSON.parse(result);
            this.setState({type: transactionDetails.type})
        });
    }


    render() {
        return (
            <View style={styles.container}>
                {renderIf(this.state.type === 'deposit', 
                    <DepositTransaction navigator={this.props.navigator}/>
                )}
                
                {renderIf(this.state.type === 'payment',
                    <PaymentTransaction navigator={this.props.navigator}/>
                )}

                {renderIf(this.state.type === 'transfer',
                    <TransferTransaction navigator={this.props.navigator}/>
                )}

                {renderIf(this.state.type === 'send',
                    <SendTransaction navigator={this.props.navigator}/>
                )}
            </View>
        )
    }
};

styles = {
    container: {
        flex : 1,
    },
}
module.exports = TransactionDetails  