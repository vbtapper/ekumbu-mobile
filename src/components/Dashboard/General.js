import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator, Image, AsyncStorage, Alert, ListView, Dimensions, RefreshControl } from 'react-native';

const config                    = require("../utils/Configuration/config");
const SideMenu                  = require('react-native-side-menu');

import MenuButton from "../utils/SideMenu/MenuButton"
import Menu from "../utils/SideMenu/Menu"

const windowDimensions = Dimensions.get('window');

export default class General extends Component {    
    
    constructor(props) {
        super(props);

        this.state = {
            animating: false,
            sessionToken : '',
            cardN: '',
            cardExp: '',
            cardCNumber: '',
            name: '',
            amountAvailable: '',
            loaded: false,
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            refreshing: false,
        }     
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem('LoggedCustomer', (err, result) => {
                var LoggedCustomer = JSON.parse(result);
                this.setState({sessionToken: LoggedCustomer.sessionToken})
                this.GetCardInfo();
                this.GetAmountAvailable();
                this.GetCustomerEvents();
            });
                
        }
        catch (error) {
            this.setState({animating: false})
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
        }
     }

     toggle() {
        this.setState({
          isOpen: !this.state.isOpen,
        });
    }

    updateMenuState(isOpen) {
        this.setState({ isOpen, });
    }

    onMenuItemSelected = (item) => {
        this.setState({
            isOpen: false,      
            selectedItem: item,
        });
        this.props.navigator.replace({ id: item });
    }
    
    TruckString(value, n) {
        return (value.length > n/7) ? value.substr(0, (n/7)-2) + '...' : value;
    }

    renderLoadingView() {
        return (
        <View style={styles.container}>
            <Text>
                Carregando... 
            </Text>
        </View>
        );
    }

    onRefresh() {
        this.setState({refreshing: true});
        this.GetCustomerEvents();
    }

    render() {
        if (!this.state.loaded) {
            return this.renderLoadingView();
        }
        return (  
            <View style={styles.page}>
                <View style={{backgroundColor: '#D1D1BA', width: windowDimensions.width, justifyContent: 'center', alignSelf: 'center', padding: 10}}>
                    <View style={styles.cardContainer}> 
                        <Image source={require('../../images/mlogo200.png')} style={styles.cardLogo} />
                        <Text style={styles.carName}>{this.state.name}</Text>
                        <Text style={styles.cardNumber}>{this.state.cardN}</Text>
                        <Text style={styles.cardExpDate}>{this.state.cardExp}</Text>
                        <Text style={styles.cardControlNumber}>{this.state.cardCNumber}</Text>
                    </View>
                </View>
                <View style={styles.amountContainer}>
                    <Text style={styles.amountLabel}>Montante Disponivel</Text>
                    <Text style={styles.amount}>{this.state.amountAvailable}</Text>
                </View>
                <View style={styles.transactionsDescriptions}>
                    <Text style={styles.latestTransferLabel}>ultimas transactions</Text>
                </View>
                <ScrollView style={{width: windowDimensions.width}} showsHorizontalScrollIndicator={false}>
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                    <View style={styles.transactionsArea}>
                        <ListView dataSource={this.state.dataSource} renderRow={this.renderTransations} style={styles.listView} automaticallyAdjustContentInsets={true}>
                            
                        </ListView>
                    </View>
                    <View style={styles.activityLink}>
                        <TouchableOpacity onPress={() => this.onPressAllTransactions()} >
                            <Text style={styles.buttonActivities}>ver todas</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>  
        );
    }

    onPressDetails(transID, typeParam) {
        try {
            let transactionDetail = {
                id: transID,
                type: typeParam,
                backroute: 'first'
            };

            AsyncStorage.setItem('TransactionDetails', JSON.stringify(transactionDetail), () => {
                this.props.navigator.replace({id: 'transdetails'});
            });
            
        } catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
        }
        
    }

    onPressAllTransactions() {
        this.props.navigator.replace({id: 'alltransactions'});
    }

    renderTransations = (transaction) => {
        var icon = null;

        if(transaction.type === 'deposit') {
            icon = require('../../images/depositDetails.png') 
        }
        else if(transaction.type === 'payment') {
            icon = require('../../images/paymentDetails.png') 
        }
        else if(transaction.type === 'send') {
            icon = require('../../images/payments.png') 
        }
        else {
            icon = require('../../images/transferDetails.png')
        }

        return (
            <TouchableOpacity onPress={() => this.onPressDetails(transaction.details.id, transaction.type)}>
                <View>
                    <View style={styles.rowContainer}>          
                        <View style={styles.listIconArea}>
                            <Image source={icon} style={styles.listIcon} />
                        </View>
                        <View style={styles.listSource} >
                            <View style={styles.listSourceNameArea}>
                                <Text style={styles.listData}>{this.TruckString(transaction.source_info.name, windowDimensions.width-180)}</Text>
                            </View>
                            <View style={styles.listStatus}>
                                <Text style={styles.listData}>{transaction.type} - </Text>
                                <Text style={styles.listData}>{this.TruckString(transaction.status, windowDimensions.width-180)}</Text>
                            </View>
                            <View style={styles.listDateHourData}>
                                <Text style={styles.listData}>{transaction.date}, </Text>
                                <Text style={styles.listData}>{transaction.hour}</Text>
                            </View>
                        </View>
                        <View style={styles.listAmount}>
                            <Text style={styles.listDataAmount}>{transaction.amount}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    } 

    FormatCardNumber(number) {
        return number.replace(/(.{4})/g,"$1 ")
    }

    async GetCardInfo() {
        this.setState({animating: true})
        try {
            let response = await fetch(config.API.baseurl + "/api/v1/customer/getccardinf", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                },
                body: JSON.stringify({
                    sessiontoken : this.state.sessionToken,
                    livemode: config.API.livemode,
                })
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                this.setState({
                    cardN: this.FormatCardNumber(responseJson.cardnumber),
                    cardExp: responseJson.cardexpdate,
                    cardCNumber: responseJson.cardlivetoken,
                    name : responseJson.cardholdername.toUpperCase(),
                })
            }
            else {
                this.setState({animating: false})
                Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
            }
        } 
        catch(error) {
            this.setState({animating: false})
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");   
        }
    }

    async GetAmountAvailable() {
        this.setState({animating: true})
        var adtUrl = "/api/v1/customer/getbalance?" + "sessiontoken=" + this.state.sessionToken + "&livemode=" + config.API.livemode
        try {
            let response = await fetch(config.API.baseurl +  adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                },
            })
            let responseJson = await response.json();
            this.setState({animating: false})
            if(responseJson.statusCode == 200) {
                this.setState({
                    amountAvailable: responseJson.balance,
                })
            }
            else {
                this.setState({animating: false})
                Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde');
            }
        } 
        catch(error) {
            this.setState({animating: false})
            Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde');   
        }
    }

    async GetCustomerEvents() {
        this.setState({animating: true})
        var adtUrl = "/api/v1/customer/cgetallevt?" + "sessiontoken=" + this.state.sessionToken + "&livemode=" + config.API.livemode
        try {
            let response = await fetch(config.API.baseurl + adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                }
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseJson.events),
                    loaded: true,
                    refreshing: false
                })
            }
            else {
                this.setState({animating: false, refreshing: false})
                Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 1");
            }
        } 
        catch(error) {
            this.setState({animating: false, refreshing: false})
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 2");   
        }
    }

}

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef',
        alignItems : 'center',
        height: windowDimensions.height
    },
    pageContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems : 'center'
    },
    cardContainer: {
        flex: 0,
        marginTop: 6,
        width: 270,
        height: 180,
        backgroundColor: '#1b1d1f',
        alignItems : 'flex-end',
        justifyContent: 'flex-end',
        alignSelf: 'center',
        opacity: 0.7
    },
    cardLogo: {
        height: 60,
        width: 56,
    },
    cardNumber: {
        fontSize: 18,
        fontFamily: "AppleSDGothicNeo-Regular", 
        color: 'white',
        justifyContent: 'center',
        marginRight: 38,
        marginTop: 8,
        fontWeight: '500',
        textShadowOffset: {
            width: 70,
            height: 30
        },
        textShadowColor : 'black',
        textShadowRadius: 30
    },
    carName: {
        fontFamily: "AppleSDGothicNeo-Regular", 
        fontSize: 14,
        color: 'white',
        justifyContent: 'center',
        marginRight: 123,
        marginTop: 5,
        fontWeight: '500',
        textShadowOffset: {
            width: 70,
            height: 30
        },
        textShadowColor : 'black',
        textShadowRadius: 30
    },
    cardExpDate : {
        fontFamily: "AppleSDGothicNeo-Regular", 
        fontSize: 14,
        color: 'white',
        justifyContent: 'center',
        marginRight: 150,
        marginTop: 3,
        fontWeight: '500',  
        textShadowOffset: {
            width: 70,
            height: 30
        },
        textShadowColor : 'black',
        textShadowRadius: 30
    },
    cardControlNumber: {
        fontSize: 10,
        color: 'white',
        justifyContent: 'center',
        marginRight: 5,
        marginTop: 35,
        fontWeight: '500',
        textShadowOffset: {
            width: 70,
            height: 30
        },
        textShadowColor : 'black',
        textShadowRadius: 30
    },
    amountContainer: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems : 'center',
    },
    amount: {
        fontSize: 19,
        fontWeight: '500',
    },
    amountLabel : {
        fontSize: 11
    },
    activityLink: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5
    },
    buttonActivities: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    listTransact: {
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 4
    },
    listDateHour: {
        alignItems: 'flex-start',
        alignSelf: 'center',
        width: 80
    },
    listDateHourData: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
        marginTop: 7
    },
    listSource: {
        width: windowDimensions.width-180,
        alignItems: 'flex-start',
        marginLeft: 12,
    },
    listAmount :{
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        height: window.innerHeight,
        paddingLeft: 4,
        paddingRight: 4,
    },
    transactionsDescriptions: {
        marginTop: 10,
        marginBottom : 3
    },
    latestTransferLabel : {
        fontSize: 11,
    },
    listView: {
        flex: 1,
        width: windowDimensions.width,
        height: window.innerHeight
    },
    listIcon: {
        padding: 5,
        width: 30,
        height: 30,
    },
    listIconArea: {
        alignSelf: 'center',
        marginTop: 2,
        marginLeft: 6
    },
    transactionsArea: {
        flexDirection:'row', 
        width: windowDimensions.width,
        padding: 10,
        height: window.outerHeight
    },
    separator: {
        height: 1,
        backgroundColor: '#dddddd'
    },
    rowContainer: {
        width: window.outerWidth,
        backgroundColor: '#D1D1BA',
        flexDirection: 'row',
        flex: 1,
        padding: 2,
        borderRadius:5,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 3,
        margin: 4
    },
    listSourceName: {
        height: window.innerHeight,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    listStatus: {
        marginTop: 7,
        flexDirection: 'row',
    },
    listSourceNameArea: {
        marginTop: 4,
        flexDirection: 'row',
    },
    listData: {
        fontSize: 13,
        justifyContent: 'center',
        alignItems: 'center',
    },
    listDataAmount: {
        fontWeight: 'bold',
        fontSize: 12,
        justifyContent: 'center',
        alignItems: 'center',   
        alignSelf: 'center'
    }
 }