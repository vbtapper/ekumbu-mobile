import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator, Image, Alert, AsyncStorage, ListView, Dimensions } from 'react-native';

import DetailMenu from "./DetailMenu"
import TransactionDetailsMenu from './AllTransactionsMenu'
import moment from 'moment';

const config = require("../utils/Configuration/config");
const windowDimensions = Dimensions.get('window');

var valArrayOptionsParam;
var hasAll = false;

class AllTransactions extends Component {

    constructor(props) {
        super(props);
        
        var dateInit = new Date();
        dateInit.setDate( dateInit.getDate() - 30 );
        hasAll = false;
        
        this.state = {
            sessionToken: '',
            selectedStartDate: dateInit,
            selectedEndDate: new Date(),
            selectedOptions: [{ label: 'Todas', value: 'all' }],
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
        }
        
    }

    TruckString(value, n) {
        return (value.length > n/7) ? value.substr(0, (n/7)-2) + '...' : value;
    }

    componentDidMount() {
            try {
                AsyncStorage.getItem('LoggedCustomer', (err, result) => {
                    var LoggedCustomer = JSON.parse(result);
                    this.setState({sessionToken: LoggedCustomer.sessionToken})
                });
                AsyncStorage.getItem('TransactionOptions', (err, result) => {
                    if(result != null || result != undefined) {
                        var transactionOptions = JSON.parse(result);
                        this.setState({selectedStartDate: transactionOptions.selectedStartDate, selectedEndDate: transactionOptions.selectedEndDate, selectedOptions: transactionOptions.selectedOptions})
                        valArrayOptionsParam = new Array();
                        this.state.selectedOptions.map(function(opts, i) {
                            valArrayOptionsParam.push(opts.value)
                            if(opts.value == 'all') {
                                hasAll = true;
                            }
                            else {
                                hasAll = false;
                            }
                        })
                    }
                    if(hasAll) {
                        this.GetAllTransactions()
                    }
                    else {
                        this.GetAllTransactionsByType()
                    }
                });
                
            }
            catch (error) {
                Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde 1");
            }
        }

  BackToMain() {
    this.props.navigator.replace({id: 'first'})
  }

  GoToOptions() {
      
      try {
            let transactionOptions = {
                selectedOptions: this.state.selectedOptions,
                selectedStartDate: this.state.selectedStartDate,
                selectedEndDate: this.state.selectedEndDate,
            };

            AsyncStorage.setItem('TransactionOptions', JSON.stringify(transactionOptions), () => {
                this.props.navigator.push({id: 'transactionsoptions'})
            });
            
        } catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
        }
  }

    DisplayTypes() {
        return this.state.selectedOptions.map(function(types, i){
            return(
            <View key={i} style={{marginLeft: 10}}>
                <Text style={styles.typeOption}>{types.label}</Text>
            </View>
            );
        });
    }

    DisplayDates() {
        return (
            <View>
                <Text style={styles.typeOption}>{moment(this.state.selectedStartDate).format('DD-MM-YYYY')}</Text>
                <Text style={styles.typeOption}>{moment(this.state.selectedEndDate).format('DD-MM-YYYY')}</Text>
            </View>
        );
    }

    render() {
        return (
        <View style={styles.page}>
            <View style={styles.detailMenu}>
                <TransactionDetailsMenu handlePressBack={() => this.BackToMain()} handlePressOptions={() => this.GoToOptions()} />
            </View> 
            <ScrollView showsHorizontalScrollIndicator={false}>
                <View style={styles.typesLayout}>
                    <View>
                        {this.DisplayTypes()}
                    </View>
                    <View>
                        {this.DisplayDates()}
                    </View>
                </View>
                <View>
                    <ListView dataSource={this.state.dataSource} renderRow={this.renderTransations} style={styles.listView} automaticallyAdjustContentInsets={false}/>
                </View>
            </ScrollView>
        </View> 
        );
    }

    renderTransations = (transaction) => {
        var icon = null;

        if(transaction.type === 'deposit') {
            icon = require('../../images/depositDetails.png') 
        }
        else if(transaction.type === 'payment') {
            icon = require('../../images/paymentDetails.png') 
        }
        else if(transaction.type === 'send') {
            icon = require('../../images/payments.png') 
        }
        else {
            icon = require('../../images/transferDetails.png')
        }

        return (
            <TouchableOpacity onPress={() => this.onPressDetails(transaction.details.id, transaction.type)}>
                <View>
                    <View style={styles.rowContainer}>          
                        <View style={styles.listIconArea}>
                            <Image source={icon} style={styles.listIcon} />
                        </View>
                        <View style={styles.listSource} >
                            <View style={styles.listSourceNameArea}>
                                <Text style={styles.listData}>{this.TruckString(transaction.source_info.name, windowDimensions.width-180)}</Text>
                            </View>
                            <View style={styles.listStatus}>
                                <Text style={styles.listData}>{transaction.type} - </Text>
                                <Text style={styles.listData}>{transaction.status}</Text>
                            </View>
                            <View style={styles.listDateHourData}>
                                <Text style={styles.listData}>{transaction.date}, </Text>
                                <Text style={styles.listData}>{transaction.hour}</Text>
                            </View>
                        </View>
                        <View style={styles.listAmount}>
                            <Text style={styles.listDataAmount}>{transaction.amount}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    } 

    onPressDetails(transID, typeParam) {
        try {
            let transactionDetail = {
                id: transID,
                type: typeParam,
                backroute: 'alltransactions'
            };

            AsyncStorage.setItem('TransactionDetails', JSON.stringify(transactionDetail), () => {
                this.props.navigator.replace({id: 'transdetails'});
            });
            
        } catch (error) {
            Alert.alert('ekumbu', "Ocorreu um erro interno, por favor tente mais tarde");
        }
        
    }

    async GetAllTransactions() {
            var adtUrl = "/api/v1/customer/cgetallevtnolimit?" + "sessiontoken=" + this.state.sessionToken + "&livemode=" + config.API.livemode + "&from=" + moment(this.state.selectedStartDate).format('MM-DD-YYYY') + "&to=" +  moment(this.state.selectedEndDate).format('MM-DD-YYYY')
            try {
                let response = await fetch(config.API.baseurl +  adtUrl, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        "Authorization" : config.API.key
                    },
                })
                let responseJson = await response.json();
                if(responseJson.statusCode == 200) {
                    this.setState({
                        dataSource: this.state.dataSource.cloneWithRows(responseJson.events)
                    })
                }
                else {
                    Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 1');
                }
            } 
            catch(error) {
                Alert.alert(error.message)
                Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 2');   
            }
    }  

    async GetAllTransactionsByType() {

        var adtUrl = "/api/v1/customer/cgetallevttypemulti?" + "sessiontoken=" + this.state.sessionToken + "&livemode=" + config.API.livemode + "&from=" + moment(this.state.selectedStartDate).format('MM-DD-YYYY') + "&to=" +  moment(this.state.selectedEndDate).format('MM-DD-YYYY') + "&types=" + JSON.stringify(valArrayOptionsParam) 
        
        try {
            let response = await fetch(config.API.baseurl +  adtUrl, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization" : config.API.key
                },
            })
            let responseJson = await response.json();
            if(responseJson.statusCode == 200) {
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseJson.events)
                })
            }
            else {

                Alert.alert('ekumbu', responseJson.message);
            }
        } 
        catch(error) {
            Alert.alert(error.message)
            Alert.alert('ekumbu', 'Ocorreu um erro interno, por favor tente mais tarde 2');   
        }
    }  
}

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#f4f3ef'
    },
    transactionSection: {
        flex: 1
    },
    transDetailsBanner: {
        flex: 0,
        width: window.width,
        height: 200,
        backgroundColor: '#E6E6D9',
        alignItems : 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        opacity: 0.9
    },
    transDetailsBannerLineTwo: {
        fontSize: 9,
        marginTop: 5,
        color: 'black',
        fontWeight: 'bold'
    },
    transDetailsBannerLineOne: {
        fontSize: 11,
        color: 'black',
        marginTop: 8,
        fontWeight: 'bold'
    },
    transDetailsBannerIcon: {
        height: 60,
        width: 60,       
    },
    transDetailsFrom: {
        padding: 10,
        flexDirection: 'row',
        width: window.width,
        justifyContent: 'space-around',
    },
    detailMenu: {
        width: window.width,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    transDetailsFromColumnLabel: {
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignItems: 'flex-start',
        flex: 1
    },

    transDetailsFromColumnValue: {
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flex: 1
    },
    separator: {
        height: 1,
        backgroundColor: '#dddddd'
    },
    transDetailsValue: {
        fontSize: 12
    },
    typeOption: {
        fontSize: 12,
        backgroundColor: '#E8FFF0',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff',
        height: window.outerHeight,
        width: window.innerWidth,
        padding: 5,
    },
    typesLayout: {
        flexDirection: 'row',
        flex: 1,
        padding: 20,
    },
    transactionsArea: {
        flexDirection:'row', 
        padding: 10,
        height: window.outerHeight
    },
    listView: {
        flex: 1,
        height: window.innerHeight
    },
    rowContainer: {
        width: window.outerWidth,
        backgroundColor: '#D1D1BA',
        flexDirection: 'row',
        flex: 1,
        padding: 2,
        borderRadius:5,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 3,
        margin: 4
    },
    listDateHour: {
        alignItems: 'flex-start',
        alignSelf: 'center',
        width: 80
    },
    listDateHourData: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
        marginTop: 7
    },
    listData: {
        fontSize: 13,
        justifyContent: 'center',
        alignItems: 'center',
    },
    listIconArea: {
        alignSelf: 'center',
        marginTop: 2,
        marginLeft: 6
    },
    listIcon: {
        padding: 5,
        width: 30,
        height: 30,
    },
    listSource: {
        width: windowDimensions.width-180,
        alignItems: 'flex-start',
        marginLeft: 12,
    },
    listSourceNameArea: {
        marginTop: 4,
    },
    listStatus: {
        marginTop: 7,
        flexDirection: 'row',
    },
    listAmount :{
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        height: window.innerHeight,
        paddingLeft: 4,
        paddingRight: 4,
    },
    listDataAmount: {
        fontWeight: 'bold',
        fontSize: 12,
        justifyContent: 'center',
        alignItems: 'center',   
        alignSelf: 'center'
    }
}

module.exports = AllTransactions