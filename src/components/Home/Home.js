import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, TouchableOpacity, View, Navigator } from 'react-native';

import Login from "../Login/Login"
import Register from "../Register/Register"
import Welcome from "../Welcome/Welcome"
import MainDashboard from "../Dashboard/MainDashboard"

let buildStyleInterpolator = require('buildStyleInterpolator');

var NoTransition = {
  opacity: {
    from: 1,
    to: 1,
    min: 1,
    max: 1,
    type: 'linear',
    extrapolate: false,
    round: 100,
  },
};

export default class Home extends Component {
  render() {
    return (
      <Navigator
        initialRoute={{screen: 'Welcome'}}
        renderScene={(route, nav) => {return this.renderScene(route, nav)}}
        configureScene={ () => { 
          return {
            ...Navigator.SceneConfigs.HorizontalSwipeJumpFromRight, 
            gestures: false, 
            defaultTransitionVelocity: 100,
            animationInterpolators: {
              into: buildStyleInterpolator(NoTransition),
              out: buildStyleInterpolator(NoTransition),
            },
          }; 
        }}
      />
    )
  }
  renderScene(route,nav) {
    switch (route.screen) {
      case "Welcome":
        return <Welcome navigator={nav} />
      case "Login":
        return <Login navigator={nav} />
      case "Register":
        return <Register navigator={nav} />
      case "Dashboard":
        return <MainDashboard navigator={nav}/>
      }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});