/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator
} from 'react-native';

import Home from "./src/components/Home/Home"

export default class ekumbu extends Component {
  render() {
    return (
      <Home />
    )
  }
}

AppRegistry.registerComponent('ekumbu', () => ekumbu);
